%!TEX root = m.tex
\chapter{Preliminaries}

In this chapter, we build up the necessary mathematical background required to understand the reachability problem of Probabilistic Timed Automata.

% Probability Space
\section{Probability Space}

Let $\Omega$ be a set called the \emph{sample space}. A subset
$\mathcal{F} \incl 2^\Omega$ is said to be a \emph{$\sigma$-algebra}
on $\Omega$ if it satisfies the following conditions:
\begin{itemize}
\item The sample space, $\Omega \in \mathcal{F}$
\item $\mathcal{F}$ is closed under complementation: if $A_i \in
  \mathcal{F} $, then $\Omega \backslash A_i \in \mathcal{F}$
\item $\mathcal{F}$ is closed under countable union: if $A_1, A_2,
  ... \in \mathcal{F}$ then $\bigcup\limits_{i = 1}^{\infty} \in
  \mathcal{F}$
\end{itemize}
A function $P: \mathcal{F} \mapsto [0,1]$ is a \emph{probability
  measure} if the following conditions are satisfied: \cite{BIL95}
\begin{itemize}
\item $0 \leq P(A) \leq 1$ for $A \in \mathcal{F}$
\item $P(\phi) = 0$ and $P(\Omega) = 1$
\item If $A_1, A_2, ...$ is a disjoint sequence of sets in
  $\mathcal{F}$ and if ($\bigcup\limits_{i = 1}^{\infty} A_i) \in
  \mathcal{F}$
  \begin{align*}
    P(\bigcup\limits_{i = 1}^{\infty} A_i) = \sum\limits_{i =
      1}^{\infty} P(A_i)
  \end{align*}

  This property is called \emph{countable additivity} and it extends
  to \emph{finite additivity} by setting the unnecessary $A_i$'s to
  $\phi$.
\end{itemize}
$P$ is called a \emph{distribution} if $\mathcal{F} = 2^\Omega$. The
set of distributions over $\Omega$ is denoted by \Dist($\Omega$). A triple $(\Omega, \mathcal{F}, P)$ is called a \emph{probability
  space}.


% Markov Chain
\section{Markov Chains}

\begin{definition}
  A Markov chain (also known as Discrete Time Markov Chain or DTMC) is a tuple $D = (S, s_0, \pdtmc)$ where $S$ is a set of
  \emph{states}, $s_0$ is a designated \emph{initial state} and
  $\pdtmc: S \times S \mapsto [0, 1]$ such that for all $s \in S$,
  \begin{align*}
    \sum\limits_{s' \in S} \pdtmc(s, s') = 1
  \end{align*}
\end{definition}

\begin{example}
  Let us model a simple experiment using a Markov chain. We toss a
  fair coin until we get a tails. Figure~\ref{fig:markov_chain}
  illustrates the Markov chain corresponding to this experiment.

\begin{figure}[t]
  \centering
  \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node
    distance=2.8cm, semithick,initial text={}]
    \tikzstyle{every state}=[fill=white,draw=black,text=black]

    \node[initial,state] (A) {$start$}; \node[state] (B) [above right
    of=A] {$heads$}; \node[state] (C) [below right of=B] {$tails$};

    \path (A) edge node {0.5} (B) edge node {0.5} (C) (B) edge [loop
    above] node {0.5} (B) edge node {0.5} (C);

    \path (C) edge [loop above] node {1} (C);
  \end{tikzpicture}
  \caption{Example of Markov chain}
  \label{fig:markov_chain}
\end{figure}
The states of the markov chain are the possible configurations of the
system at each time step.
\end{example}


% Assume that $S=\{1, 2, \dots, n\}$. The function $\Delta$ can then
% be represented as a \emph{transition matrix}
% \begin{align*}
%   \begin{pmatrix}
%     \Delta_{11} & \Delta_{12} & \hdots & \Delta_{1n} \\
%     \Delta_{21} & \Delta_{12} & \hdots & \Delta_{2n} \\
%     \vdots & \vdots & \ddots & \vdots \\
%     \Delta_{n1} & \Delta_{n2} & \hdots & \Delta_{nn}
%   \end{pmatrix}
% \end{align*}
% where $\Delta_{ij}$ denotes the probability associated to state $j$
% by the distribution $\Delta(i)$. Since $\Delta(i)$ is a probability
% distribution, note that $\sum\limits_{j} \Delta_{ij} = 1$.

% Given a Markov chain $D = (S,s_0, \pdtmc)$ and some state $s \in S$,
% we will define a probability space $(\Omega_s, \Ff_s, P_s)$.  To do
% this, we need some preliminary definitions.

% Probability Space and measure associated with a Markov Chain

A \emph{path} in $D$ is a sequence of states $s_1, s_2, s_3, \dots$
such that for every consecutive states $s_i$ and $s_{i+1}$,
$\pdtmc(s_i, s_{i+1}) \neq 0$. For some state $s \in S$, we write
$\Path(s)$ and $\Path^*(s)$ to respectively denote the set of infinite
and finite paths in $D$, starting from $s$.

For $\omega \in \Path^*(s)$, we define \emph{cylinder set} of
$\omega$, $\mathcal{C}(\omega) = \{\omega' \in \Path(s) \mid
\omega \text{ is a prefix of } \omega'\}$. In other words,
$\Cc(\omega)$ gives the set of all infinite path extensions of
$\omega$.

For a finite path $\omega = s_0s_1s_2\hdots s_n$, we write $\Prob_s(\omega)$ to denote:
\begin{align*}
  \Prob_s(\omega) & = 1 && \text{ if } \omega = s \\
  \Prob_s(\omega) & = \pdtmc(s_0, s_1) \cdot \pdtmc(s_1, s_2) \cdots
  \pdtmc(s_{k-1}, s_k) && \text{ if } s_0 = s \\
  \Prob_s(\omega) & = 0 && \text{ otherwise}
\end{align*}

\begin{definition}
  Let $D = (S, s_0, \pdtmc)$ be a Markov chain. For each state $s \in S$,
  we define a probability space $(\Omega_s, \Ff_s, P_s)$ as follows:
  \begin{itemize}
  \item the sample space $\Omega_s = \Path(s)$, the set of
    infinite paths starting from the state $s$,
    % \item the sample space $\Omega_M$ is equal to $\Path(M)$, the
    %   set of infinite paths in M,
  \item $\Ff_s$ is the smallest $\sigma$-algebra on $\Omega_s$
    containing all the cylinder sets:
      
    $\{~\Cc(\omega)~|~\omega \text{ is a finite path starting from }
    s~\}$
  \item $P_s$ is the unique probability measure obtained by defining:
    \begin{align*}
      P_s(\Cc(\omega)) := \Prob_s(\omega) \quad \text{ for every
        finite path } \omega
    \end{align*}
  \end{itemize}
\end{definition}

% % Reachability probability for MC given a set G of goal state Refering
% % from
% % http://www-i2.informatik.rwth-aachen.de/i2/fileadmin/user_upload/documents/Advanced_Model_Checking/amc_lec19.pdf
% % page 22

% \begin{definition}[Reachability probability in a Markov chain]
%   Consider a Markov chain $D = (S, s_0, \pdtmc)$ and a set $G \incl S$
%   of goal states. For some state $s \in S$, consider the probability
%   space $(\Omega_s, \Ff_s, P_s)$. We define the reachability
%   probability $\Reach(s, G)$ as:
%   \begin{align*}
%     % \Reach(s, G) & ~:=~ P_s(~\bigcup_{\mathclap{\omega \in
%     % \Path^*(s) \cap (S \setminus G)^*G}}~ \Cc(\omega)~)
%     \Reach(s, G) := P_s(~\bigcup ~\{\Cc(\omega) \mid \omega \in
%     Path^*(s) \cap (S \setminus G)^*G\}~)
%   \end{align*}
% \end{definition}

% % Refer to for algorithmic methods for computing the ReachProb
% $\Reach(s, G)$ is the probability associated to the union of cylinders
% of those finite paths, $\omega$, which start in state $s$ and end in a
% goal state without visiting any other state in $G$. Further, since
% $P_s$ is a probability measure,
% \begin{align*}
%   \Reach(s, G) & = ~~\sum_{\mathclap{\omega \in \Path^*(s) \cap (S \setminus G)^*G}} ~~P_s(~\Cc(\omega)~)\\
%   & = ~~\sum_{\mathclap{\omega \in \Path^*(s) \cap (S \setminus
%       G)^*G}} ~~\Prob_s(\omega)
% \end{align*}
% $\Path^*(s) \cap (S\backslash G)^*G$ is potentially infinite. One can
% refer to \cite{[KNP07a]
%   http://www.prismmodelchecker.org/papers/sfm07.pdf} for details on
% how this can be computed. Henceforth, we shall use the notation
% $\Reach_D(s, G)$ to denote $\Reach(s, G)$ for a Markov chain $D$.
% % The algorithm covered in that paper is the one used by Prism. Should
% % we give the fixed point algorithm just for fun?

% % http://www.prismmodelchecker.org/lectures/biss07/02-dtmcs.pdf

% % Reach<=n
% \begin{definition}[Bounded reachability probability]
%   Fix a Markov Chain $D = (S, s_0, \pdtmc)$ and a set $G \incl S$ of
%   goal states. For a state $s \in S$, consider the probability space
%   $(\Omega_s, \Ff_s, P_s)$. Let us denote the reachability probability
%   in not more than $n$ transitions as $\nReach{n}(s, G)$. Then
%   \begin{align*}
%     \nReach{n}(s, G) := P_s(~\bigcup \{\Cc(\omega) \mid \omega \in
%     Path^*(s) \cap (S \setminus G)^*G \text{ and }~|\omega| \leq n\}~)
%   \end{align*}
% \end{definition}

% %$\nReach{n}(s, G)$ is the probability associated to the union of
% %cylinders of those finite paths $\omega$ with length atmost $n$, 

% Markov Decision Processes
\section{Markov Decision Process}

A markov decision process is a generalization of a markov chain, in
the sense that it allows a non-deterministic choice between multiple distributions, from the current state.

\begin{definition}
  % Taken from [KP12]
  % http://www.prismmodelchecker.org/papers/marktoberdorf11.pdf
  A \textbf{Markov decision process} or an MDP is a tuple $M = (S,
  s_0, \Sigma, \pmdp)$ where $S$ is a set of states, $s_0 \in S$ is an
  initial state, $\Sigma$ is an alphabet representing actions and
  $\pmdp$: $S \times \Sigma \times S \mapsto [0, 1]$ s.t. for
  each $s \in S$ and $a \in \Sigma$,
  \begin{align*}
    \sum\limits_{s' \in S} \pmdp(s, a, s') \in \{ 0, 1\}
  \end{align*}
\end{definition}

An action $a$ is said to be \emph{enabled} if $\sum_{s'
  \in S} \pmdp(s, a, s') = 1$. It follows that a state and an action
uniquely determines a distribution on $S$. In any given state $s$, a
\emph{probabilistic transition} $s \on{a, \mu}$ is made by
non-deterministically choosing an enabled action $a \in \Sigma$. 
The distribution $\mu$ then gets uniquely determined by $s$ and $a$. For some
state $s' \in S$, if $\mu(s') > 0$, we write $s \on{a, \mu} s'$ to
denote the transition from $s$ to $s'$.

An \emph{infinite path} in an MDP is a sequence $\omega$ = $s_1
\on{a_1, \mu_1} s_2 \on{a_2, \mu_2} s_3 \dots$ such that for all $i$,
$\pmdp(s_i, a_i, s_{i+1}) > 0$. A finite prefix of an infinite path is
called a \emph{finite path}. The set of finite paths of an MDP $M$ is
denoted by $\Path^{*}_{M}$.

For a finite path $\omega = s_0 \xrightarrow{a_1, \mu_1} s_1
\xrightarrow{a_2, \mu_2} s_2 \hdots \xrightarrow{a_n, \mu_n} s_n$, we
write $\Prob_s(\omega)$ to denote:
%\sri{As in M.C., you can say $\Prob_s(\omega) = 1$ if $\omega = s$.}
\begin{align*}
  \Prob_s(\omega) & := 1 && \text{ if } \omega = s\\
  \Prob_s(\omega) & := \pmdp(s_0, a_1, s_1) \cdot \pmdp(s_1, a_2, s_2) \cdots
  \pmdp(s_{n-1}, a_n, s_n) && \text{ if } s_0 = s \\
  \Prob_s(\omega) & := 0 && \text{ otherwise }\\  
\end{align*}

\subsection{Adversaries}

\begin{definition}
  An adversary of an MDP $M = (S, s_0, \Sigma, \pmdp)$ is a function
  $A: \Path^{*}_M \mapsto \Sigma$.
\end{definition}

Let $Adv(M)$ denote the set of all adversaries of $M$. Informally,
given any finite path and a state, the adversary decides the action to
be taken. Observe that every adversary induces a Markov chain from the
MDP by eliminating non-determinism. We shall sometimes choose to write 
$A(\s) = \langle a, \mu \rangle$ as a shorthand for $A(\s) = a$ and $\mu$, the 
distribution uniquely determined by $\s$ and $a$.

\begin{definition}
  Let $A$ be an adversary of $M$ and $\s$ be a path such that
  $last(\s) = s$. Suppose $A(\s) = \langle a, \mu \rangle$. Then, we define
   a new adversary $A[\s]$ determined by:
  \[
  A[\s](\omega) :=
  \begin{cases}
    A(\s \xrightarrow{a, \mu} \omega') & \text{ if } \w \text{ is of the form } s \xrightarrow{a, \mu} \w' \\
    A(\omega) & \text{ otherwise }
  \end{cases}
  \]
\end{definition}

In other words, $A[\s]$ is that adversary which
behaves from $s$, how $A$ would have behaved on $s$ having seen $\s$.

Define a path $w$ in the MDP to be \emph{induced} by an adversary $A$ of the MDP if for every finite prefix ($\s \xrightarrow{a, \mu} s'$) of $w$, $A(\s) = \langle a, \mu \rangle$ and $\mu(s') > 0$. Denote the set of paths induced by $A$ from some state $s$ as $\Path_{A_M}(s)$ and the set of finite prefixes these paths as $\Path_{A_M}^{*}(s)$.

We now define a probability space so that we can talk about probabilities of the paths induced by adversaries. 

\begin{definition}
Fix an MDP $M$, an adversary $A$ of $M$ and a state $s$ in $M$. We define a probability space $(\Omega_s, \Ff_s, P_s^A)$ as follows:
\begin{itemize}
  \item the sample space $\Omega_s$ is a set of infinite paths in M starting at $s$
  \item $\Ff_s$ is the smallest $\sigma$-algebra on $\Omega_s$
    containing all the cylinder sets:
    \begin{align*}
    \{~\Cc(\omega)~|~\omega \text{ is a finite path induced by $A$ starting from }
    s~\}
    \end{align*}
  \item $P_s^A$ is the unique probability measure obtained by defining:
    \begin{align*}
      P_s^A(\Cc(\omega)) := \Prob_s(\omega) \quad \text{ for every
        finite path } \omega
    \end{align*}
\end{itemize}
\end{definition}

% \begin{definition}[Markov chain induced by an adversary]
%   Given an MDP $M = (S, s_0, \Sigma, \pmdp)$ and an adversary $A$ of
%   $M$, the DTMC corresponding to $A$ is $(S^+, s_0, \pdtmc_A)$ where
%   for $\s = s_0s_1\dots s_n$,
%   \begin{align*}
%     \pdtmc_A(\sigma,~ \sigma s_{n+1}) := \pmdp(s_n, A(\s),s_{n+1})
%   \end{align*}
% \end{definition}

\subsection{Reachability Probability}
% http://www.prismmodelchecker.org/lectures/pmc/13-mdp%20reachability.pdf

\begin{definition}[Reachability probability in an MDP]
  Consider an MDP $M = (S, s_0, \Sigma, \pmdp)$ and a set $G \incl S$
  of goal states. For some state $s \in S$ and an adversary $A$ of $M$, consider the probability
  space $(\Omega_s, \Ff_s, P_s^A)$. We define the reachability
  probability $\Reach_A(s, G)$ as:
  \begin{align*}
    % \Reach(s, G) & ~:=~ P_s(~\bigcup_{\mathclap{\omega \in
    % \Path^*(s) \cap (S \setminus G)^*G}}~ \Cc(\omega)~)
    \Reach_A(s, G) := P_s^A(~\bigcup ~\{\Cc(\omega) \mid \omega \in
    \Path_A^{*}(s) \cap (S \setminus G)^*G\}~)
  \end{align*}
\end{definition}

% Refer to for algorithmic methods for computing the ReachProb
$\Reach_A(s, G)$ is the probability associated to the union of
cylinders of those finite paths which start in state $s$ and end
in a goal state without visiting any other state in $G$; and are
finite prefixes of paths induced by $A$. 

We shall write $\Path_A^*(s,G)$ to denote $\Path_A^{*}(s) \cap (S \setminus G)^*G$.
 The set of paths in $\Path_A^*(s,G)$ of length atmost $n$ are denoted by $\Path_A^{\leq n}(s,G)$.

Since $P_s^A$ is a probability measure, 
%\todo{This varies slightly from what the guys in fortuna write. For us, w is (S minus G)*G, for them its just last(w) belongs to G}
\begin{align*}
  \Reach_A(s, G) & = ~~\sum_{\mathclap{\omega \in \Path_A^*(s,G)}} ~~P_s^A(~\Cc(\omega)~)\\
  & = ~~\sum_{\mathclap{\omega \in \Path_A^*(s,G)}} ~~\Prob_s(\omega)
\end{align*}
$\Path_A^*(s,G)$ is potentially infinite. One can
refer to \cite{KNP07a} for details on
how this can be computed. We take the machinery behind the computation of this probability as a blackbox.

\begin{definition}[Bounded reachability probability]
  Consider an MDP $M = (S, s_0, \Sigma, \pmdp)$ and a set $G \incl S$
  of goal states. For some state $s \in S$ and an adversary $A$ of $M$, consider the probability
  space $(\Omega_s, \Ff_s, P_s^A)$. Let us denote the reachability probability
  in not more than $n$ transitions as $\nReach{n}(s, G)$. Then
  \begin{align*}
    \nReach{n}_A(s, G) := P_s^A(~\bigcup \{\Cc(\omega) \mid \omega \in
    \Path_A^{\leq n}(s,G)\}~)
  \end{align*}
\end{definition}

\begin{definition}[Maximal reachability probability]
  For some adversary $A$ of $M$, if the reachability probability of $G$ from $s$ is given by $\Reach_A(s, G)$, then
  \begin{align*}
    \MaxReach(s, G)  & := \sup_{A~\in~Adv(M)} \Reach_A(s,G)
    \intertext{ and in not more than $n$ transitions, }
    \nMaxReach{n}(s, G) & := \sup_{A~\in~Adv(M)} \nReach{n}_{A}(s,
    G)
  \end{align*}
\end{definition}

\begin{lemma} [Extension] \cite{BJV10} Consider some adversary $A
  \in Adv(M)$. If $s \notin G$, we have the following results
  \begin{enumerate}
  \item For any $n \in \Nat$, $\nReach{n+1}_A(s,G) = \sum\limits_{r
      \in S} \mu(s, r) \cdot \nReach{n}_{A[s \xrightarrow{a, \mu} r]}
    (r, G)$
  \item $\Reach_A(s,G) = \sum\limits_{r \in S} \mu(s, r) \cdot
    \Reach_{A[s \xrightarrow{a, \mu} r]} (r, G)$
  \end{enumerate}
\end{lemma}

\begin{proof}
  Let us first prove result 1. Let $A(s) = a$ and $\mu$ be the
  distribution determined by $s$ and $a$. Write $\support(\mu)$ to denote
  the the support of the distribution $\mu$.
  \vfill
  \begin{align*}
    \nReach{n+1}_A (s, G) & = P^A_s (~\bigcup \{\Cc(\omega) \mid \omega \in \goodpaths{s}{G}{n+1}\}~)\\
    & = \sum\limits_{\omega \in \goodpaths{s}{G}{n+1}} P^A_s (\Cc(\omega)) \\
    & = \sum\limits_{\omega \in \goodpaths{s}{G}{n+1}} \Prob^A_s (\omega) \\
    & = \sum\limits_{r \in \support(\mu)} \sum\limits_{\bar\omega \in \goodpaths{r}{G}{n}} \Prob^A_s (s \xrightarrow{a, \mu} \bar\omega) \\
    & = \sum\limits_{r \in \support(\mu)} \sum\limits_{\bar\omega \in \goodpaths{r}{G}{n}} \pmdp^A_s(s, a, r) \cdot \Prob^A_r (\bar\omega) \\
    & = \sum\limits_{r \in \support(\mu)} \pdtmc^A_s(s, a, r) \cdot \sum\limits_{\bar\omega \in \goodpaths{r}{G}{n}} \Prob^A_r (\bar\omega) \\
    & = \sum\limits_{r \in \support(\mu)} \pdtmc^A_s(s, a, r) \cdot \sum\limits_{\bar\omega \in \goodpaths{r}{G}{n}} P^{A[s \xrightarrow{a, \mu} r]}_r (\Cc(\bar\omega)) \\
    & = \sum\limits_{r \in \support(\mu)} \pdtmc^A_s(s, a, r) \cdot \nReach{n}_{A[s \xrightarrow{a, \mu} r]} (r, G) \\
    & = \sum\limits_{r \in S} \mu(s, r) \cdot \nReach{n}_{A[s
      \xrightarrow{a, \mu} r]} (r, G)
  \end{align*}
  Proof of the second result runs along the same lines as the above
  proof, if we remove the upper bound on the length of the paths.
\end{proof}

\section{Probabilistic Timed Automata}

\subsection{Clocks, Valuations and Guards}

A \emph{clock} is a variable which is used to measure the elapse of time. Clocks have a single operation called reset, which sets the value of the clock to $0$. The reset of a clock $x$ is denoted by $\{x\}$. The set of all clocks is denoted by $\Xx$. A \emph{clock valuation} (or just \emph{valuation}) is an assignment of values to every clock in $\Xx$. The clocks we shall be considering will only be assigned non-negative real numbers ($\RR_{\geq 0}$), and hence the set of all valuations will be denoted by $\RR_{\geq 0}^\Xx$.

A \emph{guard} is a constraint on the clocks. We shall denote the set of all guards as $Guards(\Xx)$. Every $g \in Guards(\Xx)$ is constructed by the following grammar.
\[ g~~::=~~x \sim b~~|~~g \land g~~|~~\textsf{true}, \quad \text{where } x \in \Xx,~ b \in N,~\sim\ \in \{ <, \le, = ,\ge, >\} \]

For some valuation $v$, we write $v \vDash g$ when $v$ satisfies the constraints enforced by $g$.

\begin{definition} \cite{BJV10}
A \textbf{probabilistic timed automata} or a PTA is a tuple $\mathcal{A} = (L, l_{init}, \Xx, inv, edges)$ where
\begin{itemize}
  \item $L$ is a finite set of locations
  \item $l_{init}$ is the initial location
  \item $\Xx$ is a finite set of real-valued clocks
  \item $inv: L \mapsto Guards(\Xx)$ assigns an invariant to each location
  \item $edges \subseteq L \times Guards(\Xx) \times \Dist(2^{\Xx} \times L)$
\end{itemize}
\end{definition}

For an edge $(l, g, p) \in edges$, $l$ denotes the source location, $g$ denotes the guard and $p$ is  a distribution over \emph{instantaneous effects}, which consists of a set of clocks to be reset and a destination.

\subsection{Semantics}

\begin{definition}
The semantics of PTA $\mathcal{A}$, denoted by $\semantics{\mathcal{A}}$, is given by the tuple $(S, s_0, \Sigma, \pmdp$) where
\begin{itemize}
\item $S = \{ (l, v) \mid l \in L \land v \vDash inv(l)\}$, a \emph{location} and a \emph{clock valuation} pair
\item $s_0 = (l_{init}, \{ x \mapsto 0 \mid x \in \mathbb{X} \})$, \emph{initial location} with all clocks set to \emph{zero}
\item $\Sigma = \mathbb{R}_{\geq 0} \cup Guards(\Xx)$, either a \emph{real number} or a \emph{guard}
% \item $\pmdp((l, v), a) = \mu$ iff $(l, v) \in S$ and one of the following holds
%   \begin{itemize}
%     \item there exists $a \in \mathbb{R}_{\geq 0}$, $v+a \vDash inv(l)$ and $\mu(l, v+a) = 1$
%     \item or there exists $a \in Guards(\Xx)$, $(l, g, p) \in edges$ such that $v \vDash g$ and $\forall (l', v') \in S$
%     \begin{align*}\mu(l', v') = \sum_{R \incl \Xx ~s.t.~ v' = v[R:=0]} p( R, l')
%     \end{align*}
%   \end{itemize}
% \end{itemize}
\item $\pmdp((l,v), a, (l',v')) = r$ if $(l,v), (l',v') \in S$ and $r$ is given by:
\begin{itemize}
\item if $a \in Guards(\Xx)$, $(l, a, p) \in edges$ and $R \incl \Xx$ such that $v' = v[R:=0]$ and $v' \vDash a$, then $r = p(R, l')$
\item if $a \in \mathbb{R}_{\geq 0}$, $l' = l$ and $v' = v+d \vDash inv(l)$, then $r = 1$.
\item otherwise, $r = 0$
\end{itemize}
If the first condition holds, we call it a \emph{discrete transition} and if the second holds, we call it a \emph{timed transition}.
\end{itemize}
\end{definition}

Now we show that $\semantics{\mathcal{A}}$ is an MDP. It is enough to show that, $\forall ~s \in S$, $\sum_{s' \in S}~\pmdp(s, a, s')~=~\{0, 1\}$. 

If $a \in \mathbb{R}_{\geq 0}$, then by the second and third conditions, we have that $r = 0$ or $1$. So we are left to analyse what happens when $a \in Guards(\Xx)$. We note that, when $a \in Guards(\Xx)$, if we fix an edge $(l, a, p)$, then $\sum_{l' \in L} p(R, l') = 1$ since $p$ is a distribution over $(2^\Xx \times L)$. Since, for any $((l,v), a, (l', v'))$, one of the above will hold, we have $\forall ~s \in S$, $\sum_{s' \in S}~\pmdp(s, a, s')~=~\{0, 1\}$.

\subsection{Zones}

%From the definition of $\semantics{\mathcal{A}}$, we see that $S$ is potentially infinite (since $v$. In order to tackle the infiniteness of the \emph{Zones} are sets of states of $\semantics{\mathcal{A}}$

Let $X = \{x_1, x_2, \dots, x_k\}$ be a set of clocks. A \emph{zone} is a set of valuations given
by conjunctions of constraints of two kinds: $x_i \sim c$, and $x_i -
x_k \sim c$ where $\sim\ \in \{ <, \le, = ,\ge, >\}$. 

\subsection{Predecessor Operations}
The MDP given by $\semantics{\mathcal{A}}$ is an infinite MDP. To make computations feasible, we try to group the states together and obtain a finite MDP. In doing so, we ensure that the maximum reachability probability of a set of states from some state, remains the same. For this purpose, we introduce predecessor operations as follows.

\begin{definition}[Predecessor Operations]
Let $\semantics{\mathcal{A}} = (S, s_0, \Sigma, \pmdp)$. For some $e = (l,
g, p) \in edges$ and $f = (R, l') \in \support(p)$, for any zone $Z$, the discrete and timed predecessors are respectively defined
as: % \todo{Shouldn't zones have a single location? Arbitrary subsets of
%  S might contain valuations at multiple locations.}\sri{What you say
%  is correct. A zone is a set of valuations. A node in the zone graph
%  is of the form $(l, Z)$. You can rewrite the definitions according
%  to this convention - for example $dpre_{e,f}(l,Z)=\dots$}
\begin{align*}
dpre_{e,f}(l', Z') & := \{(l, v) \in S ~|~ v \vDash g ~\land~ v[R:=0]) \in Z'\}\\
tpre(l, Z)  &:= \{ (l, v) \in S ~|~ \exists d \in \mathbb{R}_{\geq 0} ~s.t.~ (v+d) \in Z \}
\end{align*}
\end{definition}

\begin{lemma}[Properties of predecessors] ~\\
\begin{enumerate}
\item If $(l, Z) = dpre_{e,f}(l', Z')$, then for all $v \in Z$, there exists $v' \in Z'$, such that $\pmdp((l, v), a, (l',v')) \neq 0$ for some $a$.\\
\item If $(l, Z) = tpre(l, Z')$, then for all $v \in Z$, there exists $v' \in Z'$ and $d \in \mathbb{R}_{\geq 0}$ such that $\pmdp((l, v), d, (l, v')) \neq 0$.
\end{enumerate}
\end{lemma}
\begin{proof}
\emph{Property 1}. Let $(l, Z) = dpre_{e,f}(l', Z')$. Let $v \in Z$ and $v' \in Z'$. From definition, for every $v$, there exists some $e = (l, g, p)$ and $f = (R, l') \neq 0$ such that $(l', v[R:=0]) \in Z'$ where $v \vDash g$.\\

\noindent\emph{Property 2}. Let $(l, Z) = tpre(l, Z')$. Let $s \in (l, Z)$ and $s' \in (l, Z')$. From definition, there exists $d \in \mathbb{R}_{\geq 0}$ such that $s \xrightarrow{d} s'$, which is same as $\pmdp(s, d, s') = 1$.
\end{proof}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "m"
%%% End: 
%!TEX root = m.tex
