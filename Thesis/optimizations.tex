%!TEX root = m.tex
\chapter{Algorithm and Optimizations}

In this chapter, we first present the basic backward analysis algorithm and discuss its working. Then, we discuss an optimization from \Fortuna~following which our contributions are presented.

\section{Backwards Analysis}

\begin{algorithm}
\caption{Basic Algorithm}
\label{alg:basic}
\begin{algorithmic}[1]
\State Initialize queue $Q := (G, true)$, $D := \emptyset$
\While{$\sigma = dequeue(Q)$}
  \For{every incoming edge $(e, f)$ of $\sigma$}
    \If{$\rho = pre_{e,f}(\sigma)$ has not been seen before}
      \State Add $\rho$ to $Q$
      \State Add $\rho \xrightarrow{e,f} \sigma$ to $D$
      \For{every $\rho' \xrightarrow{e',f'} \sigma'$ in $D$ such that $\rho \cap \rho' \neq \emptyset$} \label{alg:intersection}
        \State Add $\{(\rho \cap \rho') \xrightarrow{e,f} \sigma, (\rho \cap \rho') \xrightarrow{e',f'} \sigma'\}$ to $D$
        \State Add $(\rho \cap \rho')$ to $Q$
      \EndFor
    \EndIf
  \EndFor
\EndWhile
\end{algorithmic}
\end{algorithm}

The crux of backwards analysis lies in starting exploration from the
goal state and repeatedly applying the predecessor operations until no new state is seen. This process results in construction of a \emph{Markov decision process (MDP)}, using which, various verification questions may be answered. In this discussion, we restrict ourselves to the problem of computing the \emph{maximum reachability probability} with which a given set of states is reachable from an initial state.

\emph{Algorithm \ref{alg:basic}} gives a basic outline of the exploration process. The output of the algorithm is a reachability graph, $D$. Note that the \emph{breadth-first search (BFS) order} is only a matter of convenience. The algorithm operates on \emph{states} of the form $(l, Z)$, where $l$ is a \emph{location} of the PTA and $Z$ is a \emph{zone}. In the beginning, a BFS queue $Q$ is initialized containing the $(G, true)$ state, which is the goal location along with the $true$ zone\footnote{The \emph{true} zone is the set of all valuations ($x_i \geq 0$). Note that for the sake of simplicity, we are working with a single goal state. In case there are multiple goal states, they can be pushed into the initial queue.}. The while-loop on line 2 repeatedly dequeues $Q$, to get $\s$, on which the main operations are performed. The predecessor of $\s$ with respect to $(e,f)$ is computed for every incoming edge $(e, f)$ of $\s$ (where $e = (l, g, p)$ and $f \in \support(p)$). If the predecessor, $\r$, is non-empty, it is added to $Q$. The reachability graph $D$ is also updated with the explored edge, $\r \xrightarrow{e,f} \s$. Lines $7-9$ form a key part of the algorithm. For every state $\r'$ in $D$, if $location(\r') = location(\r)$ and their zones intersect, then a new state $\r \cap \r'$ is added with edges to successors of $\r$ and $\r'$. The intuition behind this step is as follows: there is a set of clock valuations which are common in $\r$ and $\r'$. Starting at these valuations, it is possible to take every edge going out of both $\r$ and $\r'$. Hence we add a new state which represents the common valuations. The new state is then added to $Q$.

% \section{FORTUNA Base Algorithm}

% \begin{algorithm}
% \caption*{The Algorithm}
% \begin{algorithmic}[1]
%   \State  $\sigma_{goal} :=  l_{goal} \times inv(l_{goal}) $
%   \State $\sigma_{init} :=  \{(l_{init}, \{x \mapsto 0 \ |\  x \in X\}\}$
%   \If{$\sigma_{init} \subseteq \sigma_{goal}$} \State{$\sigma_{init} = \sigma_{goal}$} \EndIf
%   \State $Visited = \{\sigma_{init}, \sigma_{goal}\}$
%   \State $D = \{\sigma_{goal} \xrightarrow{\tau, 1} \sigma_{goal}, \sigma_{init}, \xrightarrow{\tau, 1} \sigma_{init}\}$
%   \State $Waiting_0 = \sigma_{goal}$
%   \For {$i := 1$ to \textsf{maxlength}}
%     \If{$Waiting_{i-1} := \emptyset$} \textbf{break} \EndIf
%     \State $Waiting_i := \emptyset$
%     \For{\textbf{each} $\rho \in Waiting_{i-1}$}
%       \For{\textbf{each} $e \in edges$ and $f$ is and inst. effect of $e$ going to the location of $\rho$}
%       \State $\sigma := dpre_{e,f}(tpre(\rho))$
%       \If{$\sigma \neq \emptyset$}
%         \State $D := D \cup \{\sigma \xrightarrow{e,f} \rho\}$
%         \If{$\sigma \notin Visited$} 
%           \State $Waiting_i := Waiting_i \cup \{\sigma\}$
%           \State $Visited := Visited \cup \{\sigma\}$
%         \EndIf
%         \For{\textbf{each} $\sigma' \xrightarrow{e',f'} \rho' \in D$}
%           \If{$\sigma \cap \sigma' \neq \emptyset$}
%             \State $D := D \cup \{(\sigma \cap \sigma') \xrightarrow{e,f} \rho, (\sigma \cap \sigma') \xrightarrow{e',f'} \rho'\}$
%             \If{$\sigma \cap \sigma' \notin Visited$}
%               \State $Waiting_i := Waiting_i \cup \{\sigma \cap \sigma'\}$
%               \State $Visited := Visited \cup \{\sigma \cap \sigma'\}$
%             \EndIf
%           \EndIf
%         \EndFor
%       \EndIf

%     \EndFor
%     \EndFor
%   \EndFor
% \State \textbf{return} $(Visited, \sigma_{init}, D)$
% \end{algorithmic}
% \end{algorithm}

% \clearpage

\section{Optimizations from {\scshape Fortuna}}
\label{sec:fortopt}
One of the goals of our endeavour being reduction of the size of the reachability graph, we decided to stick only to optimizations which could be performed on-the-fly. To this effect, it was found that \emph{Optimization 2} of {\scshape Fortuna} \cite{FORTUNATechRep} is quite easy to implement and prunes a significant number of nodes in the reachability graph. \emph{Line \ref{alg:intersection}} of \emph{Algorithm \ref{alg:basic}} is replaced with:

\begin{algorithmic}[1]
\setalglineno{7}
\For{every $\rho' \xrightarrow{e',f'} \sigma'$ in $D$ such that $e' = e \land f' \neq f$ and if $\rho \cap \rho' \neq \emptyset$}
\EndFor
\end{algorithmic}

This optimization ensures that intersections are taken only when edges part of the same distribution are outgoing from $\rho'$ and $\rho$.

\section{More Optimizations}

It was recently shown for the Timed Automata reachability problem, that subsumption gives good gains in the exploration process \cite{herbreteau:hal-01166741}. When a new node is discovered during the exploration process, it is checked whether some existing node contains the new node. In such cases, it is not necessary to explore the smaller node. This leads one to wonder whether such an optimization could be extended to the PTA maximum reachability problem.

\subsection{First Attempt}

As a natural extension of the above subsumption optimization to PTA, we first came up with the following optimization. $IntmReach(s)$ denotes the maximum reachability probability of the goal states from $s$, in the current iteration of the reachability graph.

\begin{figure}
\centering
\begin{minipage}{.49\textwidth}
\centering
  \begin{tikzpicture}[font=\tiny]
  \draw[thick, fill=white] (0,0) circle(2);
  \draw[thin, fill=white] (0,-0.7) circle(0.4) node(G) {$G$};
  \draw[thin, fill=white] (-0.7,1.4) circle(0.33) node(r) {$\rho$};
  \draw[thin, fill=white] (1,1.2) circle(0.4) node(g) {$\gamma$};
  \draw[thin, fill=white] (-0.7,1.4) circle(0.36) node(r) {$\rho:0.6$};
  \draw[thin, fill=white] (1,1.2) circle(0.42) node(g) {$\gamma:0.8$};
  \draw[fill=none,draw=none] (0.15,1.3) node {$\subseteq$};
  \path[->,decoration={snake,amplitude=1.5}] (r) edge[decorate] node {} (G);
  \path[->,decoration={snake,amplitude=1.5}] (g) edge[decorate] node {} (G);
  \draw (1.1,1.6) -- (2.5,3.5);
  \draw (1.1,1.6) -- (0.9,4);
  \draw[fill=red!10,draw=none] (1.1,1.6) -- (2.5,3.5) -- (0.9,4) -- cycle;
  \draw (-0.7,1.73) -- (-0.2,4);
  \draw (-0.7,1.73) -- (-1.2,4);
  \draw[fill=green!10,draw=none] (-0.7,1.73) -- (-0.2,4) -- (-1.2,4) -- cycle;
  \draw[fill=green!10,draw=none,fill opacity=0.7] (1.1,1.6) -- (2.1,3.6) -- (1.2,3.9) -- cycle;
  \draw[dotted] (1.1,1.6) -- (2.1,3.6);
  \draw[dotted] (1.1,1.6) -- (1.2,3.9);
  \end{tikzpicture}
  \caption{First attempt}
  \label{fig:attemp1}
  \end{minipage}
\begin{minipage}{0.49\textwidth}
\centering
\vspace{13ex}
  \begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=2.5cm, semithick,initial text={},initial left,font=\small]
      \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
    \tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
    \tikzstyle{prob edge}=[->,>=stealth']
      \node[initial,state] (A) at (-4,0) {$s_0$}; 
      \node[state] (B) at (-2,0) {$s_1$};
      \node[state] (C) at (0,0) {$s_2$};
      \node[solid node] (P1) at (1,0.8) {};
      \node[accepting,state] (D) at (0,2) {$s_3$};
      \node[state] (E) at (2,0) {$s_4$};
      \node[solid node] (P2) at (-0.8,0.8) {};
      
      \path (C) edge node [left] {$e_4$} node [right,yshift=0.2cm,xshift=-0.2cm] {$x \leq 5$} (P2);
      \path[prob edge] (P2) edge [bend right] node [above left =-0.2cm] {$f_2, 0.5$} (B);
      \path[prob edge] (P2) edge [bend left] node {$f_1, 0.5$} (D);
      \path[prob edge] (P1) edge [bend right] node [above right] {$f_1, 0.5$} (D);
      \path[prob edge] (P1) edge [bend left] node {$f_2, 0.5$} (E);
      \path[prob edge] (A) edge node [below] {$e_1$} (B);
      \path[prob edge] (B) edge node [below] {$e_2$} (C);
      \path (C) edge node [right] {$e_3$} (P1);
  \end{tikzpicture}
  \vspace{3ex}
  \caption{Counter-example}
  \label{fig:counter}
\end{minipage}

  \end{figure}

{\vspace{3ex}
\begin{algorithm}
\caption*{Optimization Attempt 1: In place of \emph{Line 4} in Algorithm \ref{alg:basic}}
\begin{algorithmic}[1]
\setalglineno{4}
\If{$\rho = pre_{e,f}(\sigma)$ has not been seen before}
\If{$\exists \g~\rho \incl \g$ and $IntmReach(\g) > IntmReach(\rho)$}
	\State \textbf{continue}
\EndIf
\EndIf
\end{algorithmic}
\end{algorithm}
\vspace{3ex}
}

During the exploration process, if the newly discovered node $\r$ is included in some existing node $\g$ (in the current state of the reachability graph), then we skip exploring $\r$. Figure \ref{fig:attemp1} illustrates the idea.

\begin{figure}
\centering
% \begin{minipage}{.49\textwidth}
% \centering
%   \begin{tikzpicture}[font=\tiny]
%   \draw[thick, fill=white] (0,0) circle(2);
%   \draw[thin, fill=white] (0,-0.7) circle(0.4) node(G) {$G$};
%   \draw[thin, fill=white] (-0.7,1.4) circle(0.33) node(r) {$\rho$};
%   \draw[thin, fill=white] (1,1.2) circle(0.4) node(g) {$\gamma$};
%   \draw[thin, fill=white] (-0.7,1.4) circle(0.36) node(r) {$\rho:0.6$};
%   \draw[thin, fill=white] (1,1.2) circle(0.42) node(g) {$\gamma:0.8$};
%   \draw[fill=none,draw=none] (0.15,1.3) node {$\subseteq$};
%   \path[->,decoration={snake,amplitude=1.5}] (r) edge[decorate] node {} (G);
%   \path[->,decoration={snake,amplitude=1.5}] (g) edge[decorate] node {} (G);
%   \draw (1.1,1.6) -- (2.5,3.5);
%   \draw (1.1,1.6) -- (0.9,4);
%   \draw[fill=red!10,draw=none] (1.1,1.6) -- (2.5,3.5) -- (0.9,4) -- cycle;
%   \draw (-0.7,1.73) -- (-0.2,4);
%   \draw (-0.7,1.73) -- (-1.2,4);
%   \draw[fill=green!10,draw=none] (-0.7,1.73) -- (-0.2,4) -- (-1.2,4) -- cycle;
%   \draw[fill=green!10,draw=none,fill opacity=0.7] (1.1,1.6) -- (2.1,3.6) -- (1.2,3.9) -- cycle;
%   \draw[dotted] (1.1,1.6) -- (2.1,3.6);
%   \draw[dotted] (1.1,1.6) -- (1.2,3.9);
%   \end{tikzpicture}
%   \caption{First attempt}
%   \label{fig:attemp1}
%   \vspace{5ex}
%   \end{minipage}
% \begin{minipage}{0.49\textwidth}
% \centering
% \begin{tikzpicture}[font=\tiny]
%   \draw[thick, fill=white] (0,0) circle(2);
%   \draw[thin, fill=white] (0,-0.7) circle(0.4) node(G) {$G$};
%   \draw[thin, fill=white] (-0.7,1.4) circle(0.33) node(r) {$\rho$};
%   \draw[thin, fill=white] (1,1.2) circle(0.4) node(g) {$\gamma$};
%   \draw[thin, fill=white] (-0.7,1.4) circle(0.36) node(r) {$\rho:0.6$};
%   \draw[thin, fill=white] (1,1.2) circle(0.42) node(g) {$\gamma:1$};
%   \draw[fill=none,draw=none] (0.15,1.3) node {$\subseteq$};
%   \path[->,decoration={snake,amplitude=1.5}] (r) edge[decorate] node {} (G);
%   \path[->,decoration={zigzag,amplitude=1.1}] (g) edge[decorate] node {} (G);
%   \draw (1.1,1.6) -- (2.5,3.5);
%   \draw (1.1,1.6) -- (0.9,4);
%   \draw[fill=red!10,draw=none] (1.1,1.6) -- (2.5,3.5) -- (0.9,4) -- cycle;
%   \draw (-0.7,1.73) -- (-0.2,4);
%   \draw (-0.7,1.73) -- (-1.2,4);
%   \draw[fill=green!10,draw=none] (-0.7,1.73) -- (-0.2,4) -- (-1.2,4) -- cycle;
%   \draw[fill=green!10,draw=none,fill opacity=0.7] (1.1,1.6) -- (2.1,3.6) -- (1.2,3.9) -- cycle;
%   \draw[dotted] (1.1,1.6) -- (2.1,3.6);
%   \draw[dotted] (1.1,1.6) -- (1.2,3.9);
%   \end{tikzpicture}
%   \caption{Working optimization}
%   \label{fig:attempt2}
%   \vspace{5ex}
% \end{minipage}
% \begin{minipage}{0.9\textwidth}
% \centering
% 	\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=2.5cm, semithick,initial text={},initial left,font=\small]
%       \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
%     \tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
%     \tikzstyle{prob edge}=[->,>=stealth']
%       \node[initial,state] (A) at (-4,0) {$s_0$}; 
%       \node[state] (B) at (-2,0) {$s_1$};
%       \node[state] (C) at (0,0) {$s_2$};
%       \node[solid node] (P1) at (1,0.8) {};
%       \node[accepting,state] (D) at (0,2) {$s_3$};
%       \node[state] (E) at (2,0) {$s_4$};
%       \node[solid node] (P2) at (-0.8,0.8) {};
      
%       \path (C) edge node [left] {$e_4$} node [right,yshift=0.2cm,xshift=-0.2cm] {$x \leq 5$} (P2);
%       \path[prob edge] (P2) edge [bend right] node [above left =-0.2cm] {$f_2, 0.5$} (B);
%       \path[prob edge] (P2) edge [bend left] node {$f_1, 0.5$} (D);
%       \path[prob edge] (P1) edge [bend right] node [above right] {$f_1, 0.5$} (D);
%       \path[prob edge] (P1) edge [bend left] node {$f_2, 0.5$} (E);
%       \path[prob edge] (A) edge node [below] {$e_1$} (B);
%       \path[prob edge] (B) edge node [below] {$e_2$} (C);
%       \path (C) edge node [right] {$e_3$} (P1);
% 	\end{tikzpicture}
%   \caption{Counter-example}
%   \label{fig:counter}
%   \vspace{5ex}
% \end{minipage}
\begin{minipage}{.49\textwidth}
\centering
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm, semithick,initial text={},initial above,font=\small,scale=0.8]
	    \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
		\tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
		\tikzstyle{estate}=[ellipse,draw,inner sep=1,fill=none,node distance=1.8cm]
		\tikzstyle{prob edge}=[->,>=stealth']
	    \node[estate] (A) {$s_3, x \geq 0$};
	    \node[estate, node distance=2.5cm] (B) [above left of =A] {$s_2, x \geq 0$};
	    \node[estate, node distance=2.5cm] (C) [above right of =A] {$s_2, x \leq 5$};
	    \node[estate] (D) [above of =B] {$s_1, x \geq 0$};
	    \node[estate] (E) [above of =C] {$s_1, x \leq 5$};
	    \node[initial,estate] (F) [above of =D] {$s_0, x \geq 0$};
	    \node[initial,estate] (G) [above of =E] {$s_0, x \leq 5$};

	    \path[prob edge] (B) edge [bend right] node [left] {$e_3 f_1$, $\frac{1}{2}$} (A);
	    \path[prob edge] (C) edge [bend left] node [right] {$e_4 f_1$, $\frac{1}{2}$} (A);
	    \path[prob edge] (D) edge node [left] {$e_2$, 1} (B);
	    \path[prob edge] (E) edge [bend right] node [left] {$e_2$, 1} (C);
	    \path[prob edge] (C) edge [bend right] node [right] {$e_4 f_2$, $\frac{1}{2}$} (E);
	    \path[prob edge] (C) edge node [below left=-0.2cm] {$e_4 f_2$, $\frac{1}{2}$} (D);
		\path[prob edge] (F) edge node [left] {$e_1$, 1} (D);
	    \path[prob edge] (G) edge node [right] {$e_1$, 1} (E);
	\end{tikzpicture}
  \caption{Without subsumption}
  \label{fig:counterw}
  \end{minipage}
\begin{minipage}{0.49\textwidth}
\centering
  \begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm, semithick,initial text={},initial above,font=\small,scale=0.8]
	    \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
		\tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
		\tikzstyle{estate}=[ellipse,draw,inner sep=1,fill=none,node distance=1.8cm]
		\tikzstyle{prob edge}=[->,>=stealth']
	    \node[estate] (A) {$s_3, x \geq 0$};
	    \node[estate, node distance=2.5cm] (B) [above left of =A] {$s_2, x \geq 0$};
	    \node[estate] (D) [above of =B] {$s_1, x \geq 0$};
	    \node[initial,estate] (F) [above of =D] {$s_0, x \geq 0$};

	    \path[prob edge] (B) edge [bend right] node [left] {$e_3 f_1$, $\frac{1}{2}$} (A);
	    \path[prob edge] (D) edge node [left] {$e_2$, 1} (B);
		\path[prob edge] (F) edge node [left] {$e_1$, 1} (D);
	\end{tikzpicture}
  \caption{With subsumption}
  \label{fig:counters}
\end{minipage}
\end{figure}


If $\r \incl \g$, then for any edge $(e,f)$ coming into $loc(\r)$, $pre_{e,f}(\r) \incl pre_{e,f}(\g)$. A consequence of this is that the set of paths coming into $\r$ (coloured green in Figure \ref{fig:attemp1}) will be a subset of the set of paths coming into $\g$ (pink). This makes it appear as if the subsumption optimization would maintain the correctness of the algorithm. Unfortunately, this is not the case. Fig. \ref{fig:counter} gives a counter-example. The exploration of this PTA without the subsumption optimization is shown in Fig. \ref{fig:counterw} and with the subsumption is shown in Fig. \ref{fig:counters}. The maximum probability in the former case is 1, while in the latter, it is 0.5. The reason as to why the optimization fails is also evident through the figures. It turns out that that some of the paths coming into $\g$ are no longer present when $\r$, or the paths coming into $\r$ are subsumed.

\subsection{Working Optimization}

A fix to the first optimization is given below. On \emph{Line \ref{opt:editedline}}, instead of checking whether $IntmReach(\g) > IntmReach(\rho)$, we check whether $IntmReach(\g) = 1$.

{
  \vspace{3ex}
\begin{algorithm}
\caption*{Working Optimization: In place of \emph{Line 4} in Algorithm \ref{alg:basic}}
\begin{algorithmic}[1]
\setalglineno{4}
\If{$\rho = pre_{e,f}(\sigma)$ has not been seen before}
\If{$\exists \g~\rho \incl \g$ and $IntmReach(\g) = 1$} \label{opt:editedline}
	\State \textbf{continue}
\EndIf
\EndIf
\end{algorithmic}
\end{algorithm}
\vspace{3ex}
}

\begin{figure}
\centering
\begin{minipage}{0.9\textwidth}
\begin{center}
\begin{tikzpicture}[font=\tiny]
  \draw[thick, fill=white] (0,0) circle(2);
  \draw[thin, fill=white] (0,-0.7) circle(0.4) node(G) {$G$};
  \draw[thin, fill=white] (-0.7,1.4) circle(0.33) node(r) {$\rho$};
  \draw[thin, fill=white] (1,1.2) circle(0.4) node(g) {$\gamma$};
  \draw[thin, fill=white] (-0.7,1.4) circle(0.36) node(r) {$\rho:0.6$};
  \draw[thin, fill=white] (1,1.2) circle(0.42) node(g) {$\gamma:1$};
  \draw[fill=none,draw=none] (0.15,1.3) node {$\subseteq$};
  \path[->,decoration={snake,amplitude=1.5}] (r) edge[decorate] node {} (G);
  \path[->,decoration={zigzag,amplitude=1.1}] (g) edge[decorate] node {} (G);
  \draw (1.1,1.6) -- (2.5,3.5);
  \draw (1.1,1.6) -- (0.9,4);
  \draw[fill=red!10,draw=none] (1.1,1.6) -- (2.5,3.5) -- (0.9,4) -- cycle;
  \draw (-0.7,1.73) -- (-0.2,4);
  \draw (-0.7,1.73) -- (-1.2,4);
  \draw[fill=green!10,draw=none] (-0.7,1.73) -- (-0.2,4) -- (-1.2,4) -- cycle;
  \draw[fill=green!10,draw=none,fill opacity=0.7] (1.1,1.6) -- (2.1,3.6) -- (1.2,3.9) -- cycle;
  \draw[dotted] (1.1,1.6) -- (2.1,3.6);
  \draw[dotted] (1.1,1.6) -- (1.2,3.9);
  \end{tikzpicture}
  \end{center}
  \caption{Working optimization}
  \label{fig:attempt2}
\end{minipage}
\end{figure}

\subsection{Proof}

In this section, we try to prove the correctness of the subsumption optimization for PTA. Let MDP $M = (\S, \s_{init}, T)$ be generated by the unoptimized algorithm and the MDP $\o M = (\o\S, \o \s_{init}, \o T)$ be generated by the algorithm implementing subsumption. For simplicity, we omit writing $\nReach{n}_A(\s, G)$ and write $\nReach{n}_A(\s)$ instead.

\begin{lemma} For every $\s \in \S$, $A \in \Pol(M)$ and for every $n \in \Nat$, there exists $\o\s \in \o\S$ and $\o A \in \Pol(\o{M})$ such that
\begin{enumerate}
\item $\s \incl \o\s$
\item $\nReach{n}_{A}(\s) \leq \nReach{n}_{\o A}(\o\s)$
\end{enumerate}
\end{lemma}

\begin{proof}
We prove the lemma by induction on $n$. The base case, $n = 0$, is easy to analyse. It follows from timed automata analysis that for every $\sigma \in \Sigma$, there exists a $\o\s \in \o\S$ such that $\s \incl \o\s$. From this, we know that if $\s \neq \s_{goal}$, then $\nReach{0}_A(\s) = 0$ and $\nReach{0}_{\o A}(\o\s) = 0$. If $\s = \s_{goal}$, then let $\o\s$ is also a goal state. Therefore both $\nReach{0}_A(\s)$ and $\nReach{0}_{\o A}(\s)$ will be equal to $1$.

Assuming that the induction hypothesis holds for all $n \leq m$, we look at the case when $n = m+1$. Pick some $\s$ and $A$. Let $A(\s) = e$ and let $e$ have probabilistic edges $f_1, f_2, \dots f_k$ going to $\s_1, \s_2, $

By definition,
\[ \nReach{m+1}_{A}(\s) = \sum_{i = 1}^k f_i \times \nReach{m}_{A[\s \xrightarrow{e,f_i} \s_i]}(\s_i) \]

For the sake of brevity, let us denote $A[\s \xrightarrow{e,f_i} \s_i]$ by $A_i$. By induction hypothesis, for each $\s_i$ and each $A_i$, we have an $\o\s_i \supseteq \s_i$ and $\o A_i$ such that $\nReach{m}_{A_i}(\s_i) \leq \nReach{m}_{\o A_i}(\o\s_i)$.

Hence, we can write $\nReach{m+1}_{A}(\s)$ as
\[ \nReach{m+1}_{A}(\s) \leq \sum_{i = 1}^k f_i \times \nReach{m}_{\o A_i}(\o\s_i) \]

Consider each node $\o\s_i$. Either 
\begin{itemize}
\item $pre_{e,f_i}(\o\s_i) \in \o\S$ or 
\item $\o\s_i$ was subsumed by some maximal 
%(\textcolor{red}{explain what maximal means}) 
$\o\r_i$ and there exists a $B_i$ such that $\nReach{m}_{B_i}(\o\r_i) = 1$
\end{itemize}

We know that $pre_{e,f_i}(\o\s_i) \in \o\S$ and $pre_{e,f_i}(\o\r_i) \in \o\S$. Let $\o\s = \bigcap_i pre_{e,f_i}(\o\g_i)$, where $\o\g_i = \o\s_i$ if $\o\s_i$ was not subsumed and $\o\r_i$ if $\o\s_i$ was subsumed. From the algorithm, we know that $\o\s$ will have edges $(e,f_i)$ to $\o\g_i$ for every $i$. We construct an adversary $\o A$ such that $\o A(\o\s) = e$, and $A(\o\s \xrightarrow{e,f_i} \o\s_i\w) = A_i(\w)$ and $A(\o\s \xrightarrow{e,f_i} \o\r_i\w) = B_i(\w)$. It is also possible to show that $\s \incl \o\s$.

For every $i$, $\s_i \incl \o\s_i$. Therefore, 
\[ pre_{e,f_i}(\s_i) \incl pre_{e,f_i}(\o\s_i) \] 

For those $\o\s_i$ which were subsumed, we have 
\[ pre_{e,f_i}(\o\s_i) \incl pre_{e,f_i}(\o\r_i) \]

Using our earlier notation, $\o\g_i$, we have 
\[ pre_{e,f_i}(\s_i) \incl pre_{e,f_i}(\o\g_i) \]

Therefore 

\[ \bigcap_i pre_{e,f_i}(\s_i) \incl \bigcap_i pre_{e,f_i}(\o\g_i) \]

or
\[ \s \incl \o\s \]

\begin{align*}
\nReach{m+1}_{A}(\s) &\leq \sum_{i = 1}^k f_i \times \nReach{m}_{\o A_i}(\o\s_i)\\
					 &\leq \sum_{i\ :\ \o\s_i\ not\ subsumed } f_i \times \nReach{m}_{\o A_i}(\o\s_i) + \sum_{i\ :\ \o\s_i\ subsumed } f_i \times \nReach{m}_{\o B_i}(\o\r_i)\\
					 &\leq \nReach{m+1}_{\o A}(\o\s)
\end{align*}

Hence, for all $n \in \Nat$, the induction hypothesis holds.
\end{proof}

\begin{theorem} For a PTA $P = (L, l_0, \Xx, inv, edges)$, suppose the non-optimized algorithm produces the MDP $M$ and the optimized algorithm produces the MDP $\o M$. $\MaxReach(\s_{init}, \{\s_{goal}\})$ has the same value in both $M$ and $\o M$.
\end{theorem}
\begin{proof}
Given the PTA $P$ with initial state $l_0$, we add a new state $l_{init}$ and a transition $l_{init} \xrightarrow{x = 0} l_0$. Clearly, this new addition does not change the probability of reaching the goal state from $l_0$. We also note that the probability from $l_{init}$ will be the same as that from $l_0$.

Let MDP $M$ be obtained by running the unoptimized algorithm and let MDP $\o M$ be obtained by running the optimized algorithm, on the modified PTA $P$. We note that both MDPs will either contain exactly one node with the location $l_{init}$, or contain no initial node at all. In the latter case, $\MaxReach$ will be 0 in both the MDPs. %\textcolor{red}{What guarantees $\o M$ has a path to the initial state? why can't the exploration terminate without reaching the initial state? There is no guarantee. And its totally fine.}

From the previous lemma, we know that given $M$ and $\o M$, for every $\s \in \S$ and $A \in \Pol(M)$, for every $n$, there exists a $\o\s$ and $\o A \in \Pol(\o M)$ such that
\[ \nReach{n}_A(\s) \leq \nReach{n}_{\o A}(\o\s) \]

Taking $\s = \s_{init}$ and $A$ to be the adversary giving the maximum probability, the lemma gives us $\o\s_{init}$ and $\o A$ such that the above condition holds. But $\o\s_{init} = \s_{init}$ since there is only one node with the location $l_{init}$. For every $n$, $\nMaxReach{n}(\s_{init})$ in $M$ is atmost $\nMaxReach{n}(\s_{init})$ in $\o M$. $[\nMaxReach{n}(\s_{init})]_{n \in \Nat}$ is an increasing sequence converging to $\MaxReach(\s_{init})$. Therefore, $\MaxReach(\s_{init})$ in $M$ is atmost $\MaxReach(\s_{init})$ in $\o M$. But since $\o M$ is a subgraph of $M$, we know that $\MaxReach(\s_{init})$ in $M$ is atleast as much as that in $\o M$. This proves the theorem.
\end{proof}

\begin{algorithm}[p]
\caption{Final Algorithm}
\label{alg:final}
\begin{algorithmic}[1]
\State Initialize queue $Q := (G, true)$, $D := \emptyset$
\While{$\sigma = dequeue(Q)$}
  \For{every incoming edge $(e, f)$ of $\sigma$}
    \If{$\rho = pre_{e,f}(\sigma)$ has not been seen before}
		\If{$\exists \g~\rho \incl \g$ and $IntmReach(\g) = 1$}
			\State \textbf{continue}
		\EndIf
      \State Add $\rho$ to $Q$
      \State Add $\rho \xrightarrow{e,f} \sigma$ to $D$
      \For{every $\rho' \xrightarrow{e',f'} \sigma'$ in $D$ $s.t.$ $e' = e \land f' \neq f$ and $\rho \cap \rho' \neq \emptyset$}
        \State Add $\{(\rho \cap \rho') \xrightarrow{e,f} \sigma, (\rho \cap \rho') \xrightarrow{e',f'} \sigma'\}$ to $D$
        \State Add $(\rho \cap \rho')$ to $Q$
      \EndFor
    \EndIf
  \EndFor
\EndWhile
\end{algorithmic}
\end{algorithm}

\clearpage

\section{Experiments}

\begin{table}[]
\centering
\caption{Comparison of the \Fortuna-based algorithm with existing algorithms in \Prism}
\label{tab:comparison}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Model        & Property        & Args & \multicolumn{2}{|c|}{Games} & \multicolumn{2}{|c|}{Naive Backwards} & \multicolumn{2}{|c|}{\Fortuna~Opt 2} \\
\hline
             &                 &      & Graph      & Time (s)     & Graph       & Time (s)       & Graph       & Time (s)       \\
\hline
repudiation, & deadline.pctl   & 5    & 1660       & 0.281        & 77          & 0.086       & 68          & 0.074       \\
malicious    &                 & 7    & 2514       & 0.806        & 195         & 0.15        & 136         & 0.104       \\
             &                & 10   & 5641       & 2.6          & 396         & 0.334       & 238         & 0.148       \\
             &  T               & 15   & 12891      & 9.663        & 731         & 1.27        & 408         & 0.26        \\
             &                 & 20   & 25707      & 16.671       & 1066        & 3.718       & 578         & 0.395       \\
             &                 & 30   & 78886      & 59.105       & 1736        & 18.093      & 918         & 0.925       \\
             & eventually.pctl &      & 254        & 0.155        & 63          & 0.121       & 60          & 0.075       \\
\hline
zeroconf     & incorrect.pctl  &      & 27         & 0.065        & 25          & 0.065       & 25          & 0.055       \\
             & eventually.pctl &      & 26         & 0.063        & 31          & 0.076       & 31          & 0.057       \\
\hline
csma, full   & collisions.pctl & 2,2  & 613        & 0.313        & 202         & 0.18        & 202         & 0.174       \\
             &                 & 2,4  & 6385       & 1.01         & 444         & 0.252       & 444         & 0.219       \\
             &                 & 2,8  & 17929      & 2.661        & 776         & 0.346       & 776         & 0.261       \\
             &            & 4,2  & 2001       & 0.493        & 570         & 0.271       & 570         & 0.235       \\
             &     K,COL            & 4,4  & 34715      & 3.884        & 2848        & 0.914       & 2848        & 0.594       \\
             &                 & 4,8  & 238483     & 33.741       & 4129        & 1.472       & 4129        & 0.924       \\
             &                 & 6,2  & 12163      & 1.585        & 1114        & 0.473       & 1114        & 0.314       \\
             &                 & 6,4  & 193657     & 20.634       & 13860       & 8.9         & 13860       & 5.19       \\ 
             &                 & 6,8  & -          & $>$ 60            & 24918       & 17.92       & 24918       & 8.71       \\ \hline
\end{tabular}
\end{table}

The final algorithm was implemented as a new engine for PTA, in the \Prism~Model Checker. We have used the standard set of case studies bundled along with \Prism~for running benchmarks. Various maximum reachability probabilities were computed for three protocols: non-repudiation protocol for information transfer \cite{NPS13}, Zero-configuration networking (zeroconf) protocol \cite{KNPS06} and CSMA/CD protocol \cite{KNSW07}. The machine on which the benchmarks were run was equipped with an AMD FX6300 3.5GHz 6-core processor and 8GB of RAM. The three algorithms tested were the Stochastic game-based abstraction refinement method (labelled \emph{Games}), the existing \Prism~implementation of the backward analysis algorithm (labelled \emph{Naive Backwards}) and the \Fortuna-based algorithm we presented in Section \ref{sec:fortopt}. Table \ref{tab:comparison} gives the results of the comparison.

Next, we added another engine with subsumption optimization (Algorithm \ref{alg:final}) and compared it with the version without subsumption (see Table \ref{tab:subsumption}). It was found that the gains were minimal, saving 4 states in the non-repudiation protocol's \emph{deadline} property, saving 2 states for the \emph{eventually} property and none for the remaining test cases. From analysis of the generated reachability graphs, we have hypothesised that most smaller nodes in the graph are formed as intersection of existing nodes. Since such nodes have more edges out of them, the probability of reaching the goal state from such a state will be higher than that from states which subsume them.

\begin{table}[]
\centering
\caption{\Fortuna-based algorithm with and without the subsumption optimization}
\label{tab:subsumption}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
\multicolumn{1}{|l|}{Model} & \multicolumn{1}{l|}{Property} & \multicolumn{1}{l|}{Args} & \multicolumn{3}{c|}{Without Subsumption} & \multicolumn{3}{c|}{With Subsumption} \\ \hline
                            &                               &                           & Graph        & MDP         & Time        & Graph     & MDP       & Time      \\ \hline
repudiation,                & deadline.pctl                 & 5                         & 68           & 114         & 0.074       & 64            & 106       & 0.072     \\
malicious                   &                               & 7                         & 136          & 260         & 0.104       & 132           & 252       & 0.099     \\
                            &                              & 10                        & 238          & 485         & 0.148       & 234           & 477       & 0.152     \\
                            &     T                          & 15                        & 408          & 860         & 0.26        & 404           & 852       & 0.248     \\
                            &                               & 20                        & 578          & 1235        & 0.395       & 574           & 1227      & 0.417     \\
                            &                               & 30                        & 918          & 1985        & 0.925       & 914           & 1977      & 0.962     \\
                            & eventually.pctl               &                           & 60           & 106         & 0.075       & 58            & 100       & 0.079     \\
\hline
zeroconf                    & incorrect.pctl                &                           & 25           & 31          & 0.055       & 25            & 31        & 0.055     \\
                            & eventually.pctl               &                           & 31           & 37          & 0.057       & 31            & 37        & 0.057     \\
\hline
csma, full                  & collisions.pctl               & 2,2                       & 202          & 259         & 0.174       & 202           & 259       & 0.191     \\
                            &                               & 2,4                       & 444          & 633         & 0.219       & 444           & 633       & 0.227     \\
                            &                               & 2,8                       & 776          & 1157        & 0.261       & 776           & 1157      & 0.292     \\
                            &                          & 4,2                       & 570          & 687         & 0.235       & 570           & 687       & 0.262     \\
                            &      K,COL                         & 4,4                       & 2848         & 4315        & 0.594       & 2848          & 4315      & 0.906     \\
                            &                               & 4,8                       & 4129         & 6538        & 0.924       & 4129          & 6538      & 1.393     \\
                            &                               & 6,2                       & 1114         & 1291        & 0.314       & 1114          & 1291      & 0.409     \\
                            &                               & 6,4                       & 13860        & 21547       & 5.19        & 13860         & 21547     & 9.762     \\
                            &                               & 6,8                       & 24918        & 41035       & 8.71        & 24918         & 41035     & 27.651   \\ \hline
\end{tabular}
\end{table}