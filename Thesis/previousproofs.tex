\section{Previous Attempts}

Given a reachability graph Z, define a relation $\cleq$ between two nodes in the graph as follows:

\begin{align*}
s \cleq s' \quad \iff \quad s' \incl s \quad \text{and} \quad \MaxReach(s', G) \leq \MaxReach(s, G)
\end{align*}

\emph{Optimization 1} can be shown to be correct if the following lemma is true.

\begin{lemma}[Subsumption]
If $s' \cleq s$, then for every adversary $A'$ that takes the initial node $s_0$ to $s'$ with probability $\Reach_{A'}(s_0, s')$, there exists an adversary $A$ such that
\begin{align*}
\Reach_{A'}(s_0, s') \leq \Reach_{A}(s_0, s)
\end{align*}
\end{lemma}

\textbf{Property 1} Predecessor operation preserves the inclusion relation between nodes
\\

\begin{proof}
We first prove the following claim.
\\
\\
\emph{Claim}: Given $s' \cleq s$, if $r'$ is a node in the predecessor closure of $s'$ and $A'$ is some adversary, then there exists a node $r$ in the predecessor closure of $s$ and an adversary $A$ such that $\nReach{n}_{A'}(r', s') \leq \nReach{n}_{A}(r, s)$ and $r' \incl r$.
\\
\\
\emph{Proof}: We prove this claim by induction on $n$.\\
\\
\emph{Base case: $n = 1$}. We need to consider two types of nodes: predecessor nodes and intersection nodes. First, we shall examine nodes which are immediate predecessors of $s'$. Since $loc(s') = loc(s)$, the algorithm will compute predecessors of $s'$ and $s$ with respect to the same set of edges, i.e, $I = \{(e,f) \ |\  (e,f) \text{ is an incoming edge for } loc(s) \text{ in the PTA}\}$. For every edge $(e,f)$
\begin{equation*}
\begin{alignedat}{3}
&& s' &\incl s\\
\implies \quad && pre_{e,f}(s') &\incl pre_{e,f}(s) \quad &&(\text{Using \textbf{Property 1}})\\
\implies \quad && r' &\incl r \quad &&(\text{Where } r = pre_{e,f}(s))\\
\end{alignedat}
\end{equation*}

Our algorithm also adds a different type of nodes --- those resulting out of an intersection between edges of the form $(e,f_a)$ and $(e,f_b)$ where $i \neq j$. Let $r_a' = pre_{e,f_a}(s')$, $r_b' = pre_{e,f_b}(s')$ and $r_{ab}' = r_a' \cap  r_b'$. According to the algorithm, $r_{ab}'$ will have all the edges of $r_a'$ and the edges of $r_b'$ as its outgoing edges.

From the argument presented for the predecessor nodes, we know that there will exist nodes $r_a = pre_{e,f_a}(s)$ and $r_b = pre_{e,f_b}(s)$. It can be seen that \todo{is this obvious? can't the smaller node have more edges out of it?} $r_a$ will have every outgoing edge that $r_a'$ has and symmetrically for $r_b$. The algorithm will add $r_{ab} = r_a \cap r_b$ to $Z$ with all the outgoing edges of $r_a$ and $r_b$ as the outgoing edges of $r_{ab}$. Therefore, the outgoing edges of $r_{ab}$ will be a subset of the outgoing edges of $r_{ab}'$.

Suppose for some adversary $A'$ in $Z$, and for some node $r'$, $A'(r') = e_i$. Define adversary $A$ on $r$ such that $A(r) = e_i$. It is also clear that $\nReach{1}_{A'}(r',s') \leq \nReach{1}_{A}(r,s)$.\\
\\
\emph{Induction Step}: Assume that the induction hypothesis is true for $n \leq k$. We now examine the case when $n = k+1$. Consider any node $r'$ for which $\nReach{k}(r',s') \neq \nReach{k+1}(r',s')$. Let the outgoing edges of $r'$ be $e_1, e_2, \dots e_m$. Suppose, the adverary $A'$ on $r'$ gave the edge $e_i$. Say the probabilistic edges out of it are $f_1, f_2, \dots f_p$. Let $t_1', t_2', \dots t_p'$ be the respective destinations. For each $t_j'$ and adversary $A_j'$, by induction hypothesis, there exists a $t_j$ and an adversary $A_j$ such that $\nReach{k}_{A_j'}(t_j',s') \leq \nReach{k}_{A_j}(t_j,s)$. Now we define $r = \cap_{j = 1}^{p} pre_{e_i, f_j}(t_j)$. From \textbf{Property 1}, it can be seen that, for all $j$, $r' \incl pre_{e_i,f_j}(t_j)$. Hence, $r' \incl r$.

We now give a construction for a new adversary $A$.
\begin{itemize}
\item $A(r) = A'(r')$.
\item For all successors $v$ of $r$ which have a relative $v'$ in the successors of $r'$,
$A(v) = A'(v')$
\end{itemize}

This proves that for any $n$, given $s' \cleq s$, if $r'$ is a node in the predecessor closure of $s'$ and $A'$ is some adversary, then there exists a node $r$ in the predecessor closure of $s$ and an adversary $A$ such that $\nReach{n}_{A'}(r', s') \leq \nReach{n}_{A}(r, s)$ and $r' \incl r$.

Taking an node marked initial (call $s_0'$) as $r'$, we have, for all n, for every adversary $A'$,
\begin{align*}
\Reach_{A'}(s_0', s') \leq \Reach_{A}(s_0, s)
\end{align*}
where $A$ is the adversary constructed in the above proof and $s_0$ is some node which is marked initial. This proves the lemma.
\end{proof}

We construct, for every reachability graph G, a reachability graph H such that the following properties hold.
\begin{enumerate}
	\item For every node $\sigma \in H$, for every incoming edge $<e, f>$ to $loc(\sigma)$, either
	\begin{itemize}
	\item $pre(\sigma, <e, f>) \in H$ or 
	\item $\exists \sigma' \in H, \sigma' \supseteq pre(\sigma, <e, f>) $ and $ Prob(\sigma) = 1$
	\end{itemize}
	
	\item For every $\sigma, \sigma' \in H$, either
	\begin{itemize}
	\item $\sigma \cap \sigma' \in H$ or 
	\item $\exists\bar\sigma \in H, \bar\sigma \supseteq \sigma \cap \sigma',\ Prob(\bar\sigma) = 1$
	\end{itemize}
\end{enumerate}
H and G are related by the following properties.
\begin{enumerate}
\item $H \incl G$
\item For every $(l, Z) \in G$, there exists an $(l, Z') \in H$ such that Z $\incl$ Z'
\end{enumerate}
\textbf{Claim}: 
For every adversary $A$ of $G$, there exists an adversary $B$ of $H$
such that for every node $\rho = (l, Z) \in G$, there exists a node
$\sigma = (l, Z') \in H$ where $Z \incl Z'$ and 
\begin{align*}
 Prob_{A}^{\leq n}(\rho) \leq Prob_{B}^{\leq n}(\sigma) \quad \text{or} \quad Prob_{B}(\sigma) = 1
\end{align*}
\textbf{Proof}: By Induction:

Base case ($n = 0$): For every adversary $A$, $Prob_{A}^{\leq
  0}(\sigma_{goal}) = 1$ and for all other nodes, it is 0.  The same
$\sigma_{goal}$ exists in H.  Hence the hypothesis is
true.

\todo{Completeness Lemma} % For every (l, Z) in G, there exists an (l,
                          % Z') in H such that Z \incl Z'

Suppose the hypothesis holds for $n = k$. We examine the case of $n = k+1$.\newline\newline
Pick some $\rho = (l, Z) \in G$ and some adversary $A$ of $G$. We need to show that there exists a $\sigma = (l, Z' \supseteq Z) \in H$ and adversary B such that the hypothesis holds.\newline\newline
The adversary $A$ gives us some edge $e$ to take from $\rho$. Suppose the $e$-successors of $\rho$ are $p_i$, $i = 1,2,..k$. That is,
\begin{center}$\rho \xrightarrow{<e, f_i>} \rho_i, \quad i = 1,2,..k$\end{center}
The induction hypothesis gives us, for each $\rho_i$, some $\sigma_i$ and $B_i$ such that $\forall i = 1,2,..k$
\begin{center}\quad \quad \quad $Prob_A^{\leq k}(\rho_i) \leq Prob_{B_i}^{\leq k}(\sigma_i) \quad or \quad Prob_{B_i}^{\leq k}(\sigma_i) = 1$\newline
and if $\rho_i = (l, Z_i)$, then $\sigma_i = (l, Z_i' \supseteq Z_i) $\end{center}
Now we consider the $<e,f_i>$-predecessors of each $\sigma_i$. By property 1 of $H$, the following holds
\begin{center}$pre(\sigma_i,<e,f_i>) \in H$ or $\bar\sigma_i \in H$, where $\bar\sigma_i \supseteq pre(\sigma_i,<e,f_i>)$\end{center}
For the sake of brevity, let us call these predecessors or their representatives, $\hat\sigma_i$. By property 2 of $H$, the intersection of $\hat\sigma_i$'s, or a superset of it, also belongs to $H$. Let this node be $\hat\sigma$.
\begin{equation*}
\begin{split}
Prob_A^{\leq k+1}(\rho) & = \sum_{i = 1}^k f_i \times Prob_{\bar A}^{\leq k}(\rho_i) \\
 & \leq \sum_{i = 1}^k f_i \times Prob_{B_i}^{\leq k}(\sigma_i) \quad (From\ induction\ hypothesis) \\
 & \leq Prob_{B}^{\leq k+1}(\hat\sigma) 
\end{split}
\end{equation*}
where $B$ is the 1-step extension of all the $B_i$'s. Now we need to
show that $zone(\hat\sigma) \supseteq zone(\rho)$.

\section{Proving optimization 2 of FORTUNA}

\textbf{Claim}: If $ZG$ represents the zone graph generated by the original algorithm and $ZG^{opt}$ represents the zone graph generated after enabling the optimization, then
\begin{align*}
\MaxReach(s, G)[ZG] = \MaxReach(s, G)[ZG^{opt}]
\end{align*}

\begin{proof}
Let us divide the total nodes in the $ZG$ into two sets colored red and blue. We color the nodes which belong to $ZG^{opt}$ as red. We seek to isolate the nodes which are present only in $ZG$ and not in $ZG^{opt}$ and color them blue. More formally, we define a function $\chi : S \to \{Red, Blue\}$, where $S$ is the set of all nodes of the MDP.

Define $\chi$ such that,
\begin{align*}
\chi(\s) = Red \quad \iff \quad & ( \exists \r\ (pre(\r) = \s \land \chi(\r) = Red ))\ \lor\\
				& (\ \exists e \exists \s_1 \exists \s_2 \exists \r_1 \exists \r_2 \ \chi(\s_1) = Red \land \chi(\s_2) = Red\\
				& \quad \land \s_1 \cap \s_2 = \s \land  (\r_1, e, \s_1) \in E \land (\r_2, e, \s_2) \in E)\\
\end{align*}
Taking the negation of the above formula, we get a characterization for the blue nodes.
\begin{align*}
\chi(\s) = Blue \quad \iff \quad & \forall \r\ (pre(\r) = \s \Rightarrow \chi(\r) = Blue)\ \land\\
				& (\ \forall e \forall \s_1 \forall \s_2 \forall \r_1 \forall \r_2 \ ((\r_1, e, \s_1) \in E \land (\r_2, e, \s_2) \in E\\
				& \land \s_1 \cap \s_2 = \s) \Rightarrow \chi(\s_1) = Blue \lor \chi(\s_2) = Blue)
\end{align*}

Informally, a node $\s$ is blue if
\begin{itemize}
\item All nodes whose predecessor gives $\s$ are blue and 
\item For every pair of nodes which share atleast one edge with the same label, $e$, and whose intersection gives $\s$, atleast one of them is blue
\end{itemize}

Suppose the algorithm maintains the iteration, $i$ at which each blue node is added. If the blue node becomes red at some point in the future, we remove this index.

We now prove by induction that, for each $i$
\begin{align*}
\forall \s\ (\chi(\s) = Blue \Rightarrow \exists \r\ \chi(\r) = Red\ \land\ \nMaxReach{i}(\s, G) \leq \nMaxReach{i}(\r, G)\ \land\ \s \incl \r)
\end{align*}
This suffices to prove the claim.\todo{How?}\\

\noindent Base case, $i = 1$:

Pick any $\s$ such that $\chi(\s) = Blue$. Then there exists $\r$, $\chi(\r) = Red$ such that
\begin{align*}
\nMaxReach{1}(\s, G) \leq \nMaxReach{1}(\r, G) \land \s \incl \r
\end{align*}

Suppose not. Then, there exists a blue $\s'$ such that for all red $\r'$ having the same location as $\s'$
\begin{align*}
\nMaxReach{1}(\s', G) > \nMaxReach{1}(\r', G) \lor \s' \not\incl \r'
\end{align*}

But in the first iteration, a blue node is created only by the intersection of two red nodes. Hence $\s' \not\incl \r'$ can never become true. This means that $\nMaxReach{1}(\s', G) > \nMaxReach{1}(\r', G)$ is true for all $\r'$. \textbf{What can we do when the intersection gives rise to $e_if_0$ and $e_if_1$ from this blue node?}

Now suppose that 1 and 2 hold for all iterations upto $i = k$. Consider the $(k+1)^{th}$ iteration.
A blue node, call it $\s$, can come up in the $(k+1)^{th}$ iteration, in the following ways.

\begin{itemize}
\item All nodes whose $pre$ gives $\s$ are blue

Let the nodes whose $pre$ gave $\s$ be $\r_1$, $\r_2$ \dots $\r_m$. These nodes appeared in one of the previous iterations. Hence they will have an index $\leq k$ and $\s$ has an index $(k+1)$. 

By induction hypothesis, we can find $\overline\r_1$, $\overline\r_2$ \dots $\overline\r_m$ such that $\Reach$ from each of these to goal state is $\geq \Reach$ from $\r_1$, $\r_2$ \dots $\r_m$.

Each of $\overline\r_1$, $\overline\r_2$ \dots $\overline\r_m$ will have predecessors with respect to $e_1f_1$, $e_2f_2$ \dots $e_mf_m$, and the intersection of these predecessors will contain $\s$.

\begin{align*}
\Reach(\s, G) & = \Reach(pre(\overline\rho_i), G) \text{, for some $\overline\rho_i$}\\
& = f_i \times \Reach(\overline\r_i, G) \\
& \leq f_i \times \Reach(\r_i, G)
\end{align*}

\item Itersection of two nodes which don't share a common outgoing edge (this has subcases in which  the nodes are both red; one is blue and other is anything)
\item $\s$ is the intersection of a blue node and another node which have a common outgoing edge
\end{itemize}
\end{proof}