%!TEX root = m.tex

\chapter{Additional Proofs}
\begin{lemma} [Convergence of reachability probability over paths] For
  an adversary $A \in Adv(M)$, a set of goal states $G \incl S$ and a
  state $s \notin G$, the sequence $[\nReach{n}_A(s, G)]_{n \in \Nat}$
  is a non-decreasing sequence in $[0, 1]$ converging to $\Reach_A(s,
  G)$.
\end{lemma}
\begin{proof}
  Since $s$, $G$ and $A$ are fixed, for simplicity, denote
  $\Reach_A(s, G)$ by $\Reach$ and $\nReach{n}_A(s, G)$ by
  $\nReach{n}$. We shall also write $\Path_M^{A*}(s) \cap (S \setminus
  G)^*G$ as $\Xx$ and write $\Xx_{\leq n}$ to denote those elements of
  $\Xx$ having length atmost $n$.

  \textit{Claim}: $\Xx = \cup_n \Xx_{\leq n}$. \textit{Proof}. Every element $\omega \in \Xx$ is finite by definition. It will therefore be an element of $\Xx_{\leq |\omega|}$. So it is an element of $\cup_n \Xx_{\leq n}$. Now, consider $\sigma \in \cup_n \Xx_{\leq n}$. Since $\sigma \in \Xx_{\leq N}$ for some $N$, $\sigma \in \Path_M^{A*}(s) \cap (S \setminus G)^*G$. That is, $\sigma \in \Xx$. So $\Xx = \cup_n \Xx_{\leq n}$.

  We can now rewrite the definitions as follows:
  \begin{align*}
    \Reach &:= P_s^A (~ \cup \{\Cc(\omega) ~|~ \omega \in \mathbb{X}\}~) = P_s^A (\Cc(\Xx)) \\
    \nReach{n} &:= P_s^A (~ \cup \{\Cc(\omega) ~|~ \omega \in
    \mathbb{X} \land |\omega| \leq n\}~) = P_s^A (\Cc(\Xx_{\leq n}))
  \end{align*}

  We need to prove that
  \begin{align*}
    && \Reach & = \lim_{n \to \infty} \nReach{n} 
    \intertext{or} 
    && P_s^A(\Cc(\Xx)) & = \lim_{n \to \infty} P_s^A (\Cc(\Xx_{\leq n}))
    \intertext{but we have}
    &\Rightarrow & P_s^A(\Cc(\Xx)) & = P_s^A(\Cc(\Xx_{\leq n} \cup \Xx_{> n}))\\
    &\Rightarrow & P_s^A(\Cc(\Xx)) & = P_s^A(\Cc(\Xx_{\leq n}) \cup \Cc(\Xx_{> n}))\\
    &\Rightarrow & P_s^A(\Cc(\Xx)) & = P_s^A(\Xx(\Xx_{\leq n})) + P_s^A(\Cc(\Xx_{> n})) \\
    &\Rightarrow & P_s^A(\Cc(\Xx_{\leq n})) & = P_s^A(\Cc(\Xx)) - P_s^A(\Cc(\Xx_{> n}))
    \intertext{so} 
    && P_s^A (\Cc(\Xx)) & = \lim_{n \to \infty} (~P_s^A(\Cc(\Xx)) - P_s^A(\Cc(\Xx_{> n}))~)
    \intertext{$P_s^A(\Cc(\Xx))$ is a constant and
      we shall show immediately that $[P_s^A(\Xx_{> n})]_{n \in
        \Nat}$ converges. So we can write,} 
    && P_s^A (\Cc(\Xx)) & = P_s^A(\Cc(\Xx)) - \lim_{n \to \infty} P_s^A(\Cc(\Xx_{> n}))
    \intertext{Hence we
      are left to prove that} 
    && \lim_{n \to \infty} P_s^A(\Cc(\Xx_{> n})) &
    = 0
  \end{align*}

  We start by showing that $[P_s^A(\Cc(\Xx_{> n}))]_{n \in \Nat}$
  converges.

  \textit{Claim}: $\Xx_{> n+1} \incl \Xx_{> n}, \forall n \geq
  1$. \textit{Proof}. Pick an $\omega \in \Xx_{> n+1}$. So length of $\omega$ is greater than  $n+1$. Since $\Xx_{> n}$ contains all paths of greater than $n$ length, $w \in \Xx_{> n}$.

  As a consequence, we have $P_s^A(\Cc(\Xx_{> n+1})) \leq P_s^A(\Cc(\Xx_{> n}))$ for all $n \geq 1$. We also know that 0 is a lower bound for
  every element in our sequence (since it is a probability value). A
  non-increasing sequence bounded below converges. Therefore
  $[P_s^A(\Cc(\Xx_{> n}))]_{n \in \Nat}$ converges.

 If the sequence converges to some non-zero value, there exists a $k$ such that $\nReach{k}_A = \nReach{k+1}_A = \nReach{k+2}_A \dots$ and the proof follows immediately. Next, we consider the case when the sequence converges to 0. For any $n$, we can find a path, $\omega$ such that
  $|\omega| = m > n$ and $\omega \in \Xx_{> m-1}$, but $\omega \notin
  \Xx_{> m}$. So there exists an infinite subsequence $\Xx_{> i_1}
  \supset \Xx_{> i_2} \supset \Xx_{> i_3} \supset \dots$ where $i_1 < i_2
  < i_3 \dots \in \Nat$. Consequently, $P_s^A (\Cc(\Xx_{> i_1})) > P_s^A
  (\Cc(\Xx_{> i_2})) > P_s^A (\Cc(\Xx_{> i_3})) \dots$ which is a monotonically
  decreasing sequence bounded by 0. Therefore, its limit is
  0. (Suppose not. Let the limit be some $\epsilon$. But the sequence
  is infinite and monotonically decreasing. Therefore, there exists $N
  \in \Nat$ such that for all $k \geq N$, $\Xx_{i_k}$ will be less
  than $\epsilon$. But we had assumed that $\epsilon$ was the limit of
  a monotonically decreasing sequence. So it cannot be greater than
  any of the elements in the sequence. This gives us a contradiction.)

  Therefore, $\lim_{n \to \infty} P_s^A(\Cc(\Xx_{> n})) = 0$ and
  consequently, $[\nReach{n}_A(s, G)]_{n \in \Nat}$ converges to
  $\Reach_A(s, G)$.
\end{proof}

\begin{lemma} [Convergence of maximum reachability probability] \hspace{1ex}\\
  The sequence $[\nMaxReach{n}(s, G)]_{n \in \Nat}$ is a
  non-decreasing sequence in $[0, 1]$ converging to $\MaxReach(s, G)$.
\end{lemma}
\begin{proof}
  For any $n \in \Nat$, we consider that adversary $C \in Adv(M)$ for
  which $\nReach{n}_C(s, G) = \sup_{A \in Adv(M)} \nReach{n}_A(s,
  G)$. From previous lemma, we know that for every adversary $B \in
  Adv(M)$, $\nReach{n}_B(s, G) \leq \nReach{n+1}_B(s, G)$. This gives
  us
  \begin{align*}
    && \nReach{n}_C(s, G) & \leq \nReach{n+1}_C(s, G) \\
    &\text{or} & \sup_{A \in Adv(M)} \nReach{n}_A(s, G)  & \leq \nReach{n+1}_C(s,G)\\
    & \text{Therefore,} & \sup_{A \in Adv(M)} \nReach{n}_A(s, G)  & \leq \sup_{A \in Adv(M)} \nReach{n+1}_A(s, G) \\
    &\text{or} & \nMaxReach{n}(s, G) & \leq \nMaxReach{n+1}(s, G) \\
  \end{align*}

  Since $\nMaxReach{n}(s,G)$ is a probability value
  \footnote{\textsf{MaxReach} is the value of \textsf{Reach} for some
    adversary and we know that \textsf{Reach} is the value of the
    probability measure applied on a set in our event space}, for
  every $n$, it is bounded. A monotonic sequence which is bounded
  converges. Hence, $[\nMaxReach{n}(s,G)]_{n \in \Nat}$ converges.

  Let $d = \lim_{n \to \infty} \nMaxReach{n}(s,G)$. We are left to
  prove that $d$ is indeed the supremum of $\Reach_A(s,G)$ over all
  adversaries $A$ of $M$. That is,
  \begin{align*}
    \sup_A \Reach_A(s,G) & = d\\
    \text{ or } \quad \MaxReach(s,G) & = d
  \end{align*}

  \noindent \textit{Part 1}: $d \geq \MaxReach(s,G)$.
  \\\textit{Proof}.
  $[\MaxReach^{\leq n}(s, G)]_{n \in \Nat}$ is a non-decreasing sequence. Therefore the limit is greater than every term in the sequence.
  \begin{align*}
  && \forall n \quad d & \geq \MaxReach^{\leq n}(s,G)\\
  & \Rightarrow & \forall A ~\forall n \quad d & \geq \Reach_A^{\leq n}(s,G)\\
  & \Rightarrow & \forall A \quad d & \geq \lim_{n \to \infty}\Reach_A^{\leq n}(s,G)\\
  \intertext {Suppose not, then for some adversary $B$, we have $d < \Reach_B(s, G)$. However, $\MaxReach(s, G)~\geq~\Reach_B(s, G)$. It follows that $\MaxReach(s, G) > d$, which is a contradiction. Therefore,}
  && \forall A \quad d & \geq \Reach_A(s,G)\\
  & \Rightarrow & d & \geq \sup_A \Reach_A(s,G)\\
  & \Rightarrow & d & \geq \MaxReach(s,G)
  \end{align*}

  \noindent \textit{Part 2}: $d \leq \MaxReach(s, G)$.\\
  \textit{Proof}. $[\Reach_A^{\leq n}(s, G)]_{n \in \Nat}$ is a non-decreasing sequence. Therefore the limit is greater than every term in the sequence.
  \begin{align*}
  && \forall A ~\forall n \quad \Reach_A^{\leq n}(s, G) & \leq \Reach_A(s,G)\\
  & \Rightarrow & \forall A ~\forall n \quad \Reach_A^{\leq n}(s, G) & \leq \MaxReach(s,G)\\
  & \Rightarrow & \forall n ~\forall A \quad \Reach_A^{\leq n}(s, G) & \leq \MaxReach(s,G)\\
  & \Rightarrow & \forall n \quad \sup_A \Reach_A^{\leq n}(s, G) & \leq \MaxReach(s,G)\\
  \intertext{Suppose the supremum was $\epsilon$ greater than $\MaxReach(s, G)$ for some $n_0$. But we know that $\MaxReach(s, G) \geq \MaxReach^{\leq n}(s, G)$ for every $n$. So $\MaxReach(s, G) \geq \MaxReach^{\leq n_0}(s, G)$. Hence, $\MaxReach(s, G)$ would have been the supremum, which gives a contradiction. Therefore, }
  && \forall n \quad \MaxReach^{\leq n}(s, G) & \leq \MaxReach(s,G)\\
  & \Rightarrow & \lim_{n \to \infty} \MaxReach^{\leq n}(s, G) & \leq \MaxReach(s,G)\\
  \intertext{If the limit was greater than $\MaxReach(s, G)$ by some $\epsilon$, then there would exist some $\delta < \epsilon$ such that for some $n_0$, $\MaxReach^{\leq n_0}(s, G)$ is $\delta$ greater than $\MaxReach(s, G)$, which is a contradiction.}
  \end{align*}
  Therefore, $d \leq \MaxReach(s, G)$. Hence, $d =
  \MaxReach(s, G)$. In other words, the sequence $[\nReach{n}_B
  (s,G)]_{n \in \Nat}$ converges to $\MaxReach(s, G)$.

\end{proof}