%!TEX root = m.tex
\chapter{Introduction}

\section{History of Model Checking}

The ubiquity of computers and cyber-physical systems have brought with them, some unique challenges. Our dependence on them has only increased the need to build these systems with an increased rigour. Logical errors in the design of digital circuits and communication protocols are of critical importance to both engineers and programmers. Such errors may lead to the production of devices exhibiting unexpected behaviour and may cause the failure of systems already in use \cite{Clarke93verificationtools}. This scenario might lead to fatalities, when arising in the context of safely-critical systems, or cause large financial losses. The most important question which designers and testing engineers face is whether the system adheres to the specifications. This is also known as the \emph{verification} problem.

Many of these systems are naturally modelled as finite-state concurrent systems. The first efforts in formal research on solving the problem of concurrent program verification involved hand constructed proofs. In the 1970's, the use of \emph{Temporal Logic} was proposed to specify properties in concurrent programs. \emph{Model Checking}, developed by Clarke and Emerson in 1981 \cite{Clarke:1981:DSS:648063.747438}, came as an alternative automated approach to the verification problem. The specifications were expressed using propositional temporal logic and circuit designs/protocols were expressed as finite-state transition systems. A search procedure was used to check whether the specifications were satisfied by the transition systems \cite{Clarke:2008:BMC:1423535.1423536}.

\section{Quantitative Verification}

Though many systems are naturally modelled using finite state automata, the model falls short when richer constraints, such as reliability, costs, timeliness or performance, form an important aspect of design \cite{NP14}. Some examples of constraints include `chances of a component failing is less than 0.05', `number of retries must be less than 3' or `the message will always be sent within 10s'. \emph{Quantitative verification} is the process of applying formal methods in solving verification problems involving such constraints.

\subsection{Timed Automata}

Be it aerospace, health-care or communications, it is not difficult to find controllers which need to interact with physical processes, and in such systems, time plays an important role. In their seminal work \cite{Alur:1994:TTA:180782.180519}, Alur and Dill introduced an extension of the finite automata called \emph{Timed automata}. This model could successfully capture real-time behaviour of systems in which timeliness played a key role. Timed automata accept \emph{timed words} which are infinite sequences of events along with respective real-valued timestamps, denoting when each event occurred. A timed automaton is a finite state automaton with a set of finite real-valued \emph{clocks}. Each transition of the automaton could have constraints on clock values, called \emph{guards}, and a transition can be taken only when the current values of clocks satisfy the guards. On taking transitions, clocks may be independently \emph{reset}. The Timed automata formalism enables non-deterministic transition choices. As long as the guard on a transition is satisfied, the instance at which the transition is taken is a non-deterministic choice. Moreover, if more than one transitions have their guards satisfied, the choice of transition to be taken next is again non-deterministic. Timed automata can capture quantitative properties such periodicity, boundedness and delays. There are many tools (such as {\scshape Uppaal} \cite{Behrmann:2006:UPP:1173695.1173968}) which make use of timed automata for automated formal verification of real-time systems.

\begin{figure}[t]
  \centering
  \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick,initial text={},font=\small]
	    \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
		\tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
		\tikzstyle{prob edge}=[->,>=stealth']
		%\draw[step=0.5cm,color=gray] (-3,-3) grid (3,3);
		\node[state] (B) at (0,3) {$s_1 r_0$};
		\node[state] (A) at (-3,0) {$s_0 r_0$};
		\node[state] (D) at (3,0) {$s_1 r_1$};

		\path (A) edge node [xshift=0.1cm,yshift=0.5cm] {$x > 3$} node {$\{x\}$} (B);
		\path (B) edge node {$\{y\}$} (D);
		\path (D) edge [bend left] node {$y \geq 5$, $\{x\}$} (A);
		\path (A) edge node {$x > 3$} node[below] {$\{x,y\}$} (D);
	    
	\end{tikzpicture}
  \caption{Example of Timed Automaton}
  \label{fig:timed_aut}
\end{figure}

Figure \ref{fig:timed_aut} illustrates a hypothetical communication protocol. There are two parties involved, a sender $s$ and a receiver $r$. The sender sends messages over the medium and the receiver accepts the messages. It takes atleast 3 time units for the message to be sent. The state $s_0r_0$ denotes both parties being idle. $s_1r_0$ denotes the state where the sender has sent the message, but the receiver is yet to receive it. State $s_1r_1$ denotes the sender having sent the message and the receiver having received it. Whenever the message is sent by the sender, the clock $x$ is reset and when the receiver gets a message, clock $y$ is reset. Resets are denoted by surrounding the clock variable with curly braces. Finally, after waiting atleast 5 time units, both the parties return to their respective idle states.

\subsection{Probabilistic Timed Automata}

Some real-time systems exhibit behaviours which cannot be captured by timed automata. For example, on detection of an unsuccessful message transmission, a communication system might choose to wait for a random duration of time before trying again. Another situation may arise where a component may fail to trigger with a probability of 0.05. We may also wish to refer to probabilities while posing verification questions. Such requirements make a good case for a richer formalism which can model probabilistic behaviour. The need for such an extension can also be seen in communication protocols, fault-tolerant systems and in situations where one wishes to model component failures, unreliable communication media and the use of randomisation \cite{NPS13}. This led to the development of \emph{Probabilistic Timed Automata (PTA)}, an extension of the traditional timed automata \cite{KNSS02}.

Figure \ref{fig:prob_timed_aut} extends our hypothetical communication protocol to include a certain type of failure. When in state $s_1r_0$, there is a 5\% chance of the message being lost, on which the sender tries to send it again until success.

Much work has gone into development of model checking tools which make use of Probabilistic Timed Automata. In this thesis, we shall only be considering PRISM Model Checker \cite{KNP11} and {\scshape Fortuna} \cite{FORTUNATechRep}.

\begin{figure}[t]
  \centering
  \begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,semithick,initial text={},font=\small]
	    \tikzstyle{every state}=[fill=white,draw=black,text=black,scale=0.8]
		\tikzstyle{solid node}=[circle,draw,inner sep=1,fill=black]
		\tikzstyle{prob edge}=[->,>=stealth']
		%\draw[step=0.5cm,color=gray] (-3,-3) grid (3,3);
		\node[state] (B) at (0,3) {$s_1 r_0$};
		\node[state] (A) at (-3,0) {$s_0 r_0$};
		\node[state] (D) at (3,0) {$s_1 r_1$};
		\node[solid node] (P1) at (1,2) {};

		\path[prob edge] (A) edge node [xshift=0.1cm,yshift=0.5cm] {$x > 3$} node {$\{x\}$} (B);
		\path (B) edge node {} (P1);
		\path[prob edge] (P1) edge[in=60,out=25,looseness=3] node [right] {0.05} node {$\{x\}$} (B);
		\path[prob edge] (P1) edge node[left] {$\{y\}$} node[right] {0.95} (D);
		\path[prob edge] (D) edge [bend left] node {$y \geq 5$, $\{x\}$} (A);
		\path[prob edge] (A) edge node {$x > 3$} node[below] {$\{x,y\}$} (D);
	    
	\end{tikzpicture}
  \caption{Example of Probabilistic Timed Automaton}
  \label{fig:prob_timed_aut}
\end{figure}

%multimedia equipment, communication protocols, networks and fault-tolerant systems
%to component failures, unreliable communication media or the use of randomisation
%digital circuits with uncertain delay lengths, and media synchronization protocols

\section{Reachability Analysis}

A fundamental problem in safety verification is the problem of \emph{reachability}: Is it possible to reach an unsafe state from the initial state by some execution of the system. We shall be considering an analogous problem in the probabilistic setting: what is the \emph{maximum probability} with which an unsafe state can be reached from the initial state. We shall also be precisely defining what `maximum probability' means.

In regular finite automata, the reachability problem is simple to solve as we can run a graph reachability algorithm on the automata itself. In timed and probabilistic timed systems, there could be uncountably infinite possibilities (due to the dense nature of time) for when a transition can be taken. The underlying graph is therefore potentially infinite leading to complications. Hence, the most important challenge in reachability analysis of PTA is that of constructing a small and suitable finite representation of the infinite reachability graph. Various algorithms which tackle this problem have been proposed and implemented in tools such as PRISM.

\begin{enumerate}
\item \textbf{Forwards approach} This approach, proposed in \cite{KNSS02}, uses a forwards searching technique to construct a finite \emph{zone graph}. The zone graph can easily be converted into a Markov Decision Process (MDP), on which various properties can be checked. A limitation of the forwards approach is that it can only give an upper bound on the maximum probability of reaching target states.
\item \textbf{Stochastic game-based abstraction refinement} \cite{KNP09c} This method overcomes the limitations of the previous approach. The forwards approach is extended so as to obtain both lower and upper bounds for maximum probability. Further, it uses abstraction-refinement techniques to obtain results with guaranteed precision.
\item \textbf{Digital Clocks} This approach \cite{KNPS03} assumes that states are observed only at integer points and exploits this property to answer the verification problem.
\item \textbf{Backwards approach} As presented in \cite{KNS01b,FORTUNATechRep}, the backwards reachability approach used a backwards search to construct the zone graph, using which the maximum reachability probability can be easily computed. Further, for certain properties such as \emph{deadline} which refers to the the minimum probability, it is possible to make certain changes to the model and come up with an equivalent property which refers to maximum probability. The backwards approach can also model check a subset of PTCTL properties, details of which can be found in \cite{KNSW07}.
\end{enumerate}

\section{Contributions}

The game-based abstraction refinement method had been shown to be the fastest among the existing PTA verification techniques \cite{KNP09c}. A more general technique for model checking Priced Probabilistic Timed Automata, {\scshape Fortuna}, was recently shown (via benchmarks) to perform better than the game-based method in finding the maximum reachability probability \cite{FORTUNATechRep}. The goal of our study was threefold:

\begin{itemize}
\item Understand the algorithm behind {\scshape Fortuna}
\item Implement the {\scshape Fortuna} algorithm in PRISM Model Checker and compare the different algorithms on the common platform
\item Explore optimizations which can bring down the size of the reachability graph
\end{itemize}

In this thesis, we first define the maximum reachability probability precisely. Next, we present the version of the backwards reachability algorithm as implemented in PRISM, and mention a subset of optimizations we imported from the {\scshape Fortuna} algorithm. We then detail a heuristic which can be used to prune the reachability graph during construction and which gives exponential reductions in size for a certain class of PTA. Finally, the various techniques are compared using the standard set of benchmarks bundled with PRISM.